package com.dsv.road.shared.customerorder.dto;

import java.util.ArrayList;

public class DtoGoodsList {

    ArrayList<DtoGoods> list;

    public DtoGoodsList() {
        
    }
    
    public DtoGoodsList(ArrayList<DtoGoods> list) {
        this.list = list;
    }

    public ArrayList<DtoGoods> getList() {
        if (list == null)
            list = new ArrayList<DtoGoods>();
        return list;
    }

    public void setList(ArrayList<DtoGoods> list) {
        this.list = list;
    }

}
