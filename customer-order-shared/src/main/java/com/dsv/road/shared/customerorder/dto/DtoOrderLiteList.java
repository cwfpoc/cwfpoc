/**
 * 
 */
package com.dsv.road.shared.customerorder.dto;

import java.util.ArrayList;

/**
 * @author ext.jesper.munkholm
 *
 */
public class DtoOrderLiteList {

	ArrayList<DtoOrderLite> list;
	
	/**
	 * 
	 */
	public DtoOrderLiteList() {
	}

	/**
	 * @return the list
	 */
	public ArrayList<DtoOrderLite> getList() {
		if (list == null)
			list = new ArrayList<DtoOrderLite>();
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList<DtoOrderLite> list) {
		this.list = list;
	}
	
	
}
