/**
 * 
 */
package com.dsv.road.shared.customerorder.dto;

import java.math.BigDecimal;

/**
 * @author ext.jesper.munkholm
 *
 */
public class DtoCashOnDelivery {

    protected String currencyCode;
    protected BigDecimal amount;

    
    /**
	 * 
	 */
	public DtoCashOnDelivery() {
		super();
	}

	/**
     * @param currencyCode
     * @param amount
     */
    public DtoCashOnDelivery(String currencyCode, BigDecimal amount) {
		super();
		this.currencyCode = currencyCode;
		this.amount = amount;
	}

	/**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

}
