/**
 * 
 */
package com.dsv.road.shared.customerorder.dto;

/**
 * @author ext.jesper.munkholm
 * 
 */
public class DtoInstruction {
	
	private String value;

	public DtoInstruction() {
		
	}
	
	public DtoInstruction(String value) {
		this.value = value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
