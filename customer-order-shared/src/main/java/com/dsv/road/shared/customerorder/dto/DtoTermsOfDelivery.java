/**
 * 
 */
package com.dsv.road.shared.customerorder.dto;

/**
 * @author ext.jesper.munkholm
 *
 */
public class DtoTermsOfDelivery {

    protected String term;
    protected String location;

    
    /**
	 * 
	 */
	public DtoTermsOfDelivery() {
		super();
	}

	/**
	 * @param term
	 * @param location
	 */
	public DtoTermsOfDelivery(String term, String location) {
		super();
		this.term = term;
		this.location = location;
	}

	/**
     * Gets the value of the term property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTerm() {
        return term;
    }

    /**
     * Sets the value of the term property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTerm(String value) {
        this.term = value;
    }

    /**
     * Gets the value of the location property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocation() {
        return location;
    }

    /**
     * Sets the value of the location property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocation(String value) {
        this.location = value;
    }

}
