package com.dsv.road.shared.customerorder.wsclient;

import com.dsv.road.shared.customerorder.dto.DtoGoodsDetails;
import org.apache.wink.client.ClientConfig;
import org.apache.wink.client.RestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsv.road.shared.customerorder.dto.DtoOrder;
import com.dsv.road.shared.customerorder.dto.DtoOrderLiteList;
import com.dsv.shared.joda.JodaUtility;
import com.google.gson.Gson;
import java.util.ArrayList;
import javax.ws.rs.core.MediaType;
import org.apache.wink.client.ClientResponse;
import org.apache.wink.client.Resource;
/**
 * @author EXT.K.Theilgaard
 * 
 **/
public class CustomerOrderClient {

	final Logger logger = LoggerFactory.getLogger(CustomerOrderClient.class);
	
	private String restURL = "http://localhost:10039/customer_order_service-web/rest/";
        private final String ordersPath = "orders/";
        private final String ordersLitePath = "orderslite/";
        private final String orderCalculationPath = "sum/";

	private RestClient restClient = null;
	private Gson gson;

	public CustomerOrderClient() {
		ClientConfig clientConfig = new ClientConfig();
		restClient = new RestClient(clientConfig);
                gson = JodaUtility.getGson();
	}

	public CustomerOrderClient(String url) {
		this();
		this.restURL = url;
	}

	public String createCustomerOrder(DtoOrder order) {
                Resource resource = restClient.resource(restURL+ordersPath);
                resource.contentType(MediaType.APPLICATION_JSON);
                resource.accept(MediaType.APPLICATION_JSON);
                
                String json = gson.toJson(order);
                logger.info("POST (create) to endpoint ["+restURL+ordersPath+"]");
                
                ClientResponse response = resource.post(json);
                logger.debug("Response code was ["+response.getStatusCode()+"]");
                
                String responseURI = response.getHeaders().getFirst("Location");
                logger.debug("Response URI: {}", responseURI);

                if (responseURI == null || responseURI.lastIndexOf("/") == -1)
                    return null;
                
                return responseURI.substring(responseURI.lastIndexOf("/")+1);
	}	
	
	public boolean updateCustomerOrder(DtoOrder order) {
                Resource resource = restClient.resource(restURL+ordersPath+order.getOrderId());
                resource.contentType(MediaType.APPLICATION_JSON);
                resource.accept(MediaType.APPLICATION_JSON);
                
                String json = gson.toJson(order);
                logger.info("PUT (update) to endpoint [{}, {}, {}]", restURL, ordersPath, order.getOrderId());
                
                ClientResponse response = resource.put(json);
                logger.debug("Response code was ["+response.getStatusCode()+"]");

		return response.getStatusCode() == 200;
	}
	
	public DtoOrder getCustomerOrder(String orderId) {
                if(orderId == null){
                    return null;
                }
                Resource resource = restClient.resource(restURL+ordersPath+orderId);
                resource.contentType(MediaType.APPLICATION_JSON);
                resource.accept(MediaType.APPLICATION_JSON);
                
                logger.info("GET (read) to endpoint ["+restURL+ordersPath+orderId+"]");
                ClientResponse response = resource.get();
                
                String responseEntity = response.getEntity(String.class);
                DtoOrder respOrder = gson.fromJson(responseEntity, DtoOrder.class);
		return respOrder;
	}
	
	public DtoOrderLiteList getCustomerOrderInfoList() {
		Resource resource = restClient.resource(restURL+ordersLitePath);
                resource.contentType(MediaType.APPLICATION_JSON);
                resource.accept(MediaType.APPLICATION_JSON);
                
                logger.info("GET (read) to endpoint ["+restURL+ordersLitePath+"]");
                ClientResponse response = resource.get();
                
                String responseEntity = response.getEntity(String.class);
                DtoOrderLiteList resp = gson.fromJson(responseEntity, DtoOrderLiteList.class);
		return resp;
	}

        public DtoGoodsDetails calculateSum(ArrayList<DtoGoodsDetails> list) {
		// call the service
		Resource resource = restClient.resource(restURL + orderCalculationPath);
		resource.contentType(MediaType.APPLICATION_JSON);
                resource.accept(MediaType.APPLICATION_JSON);
			
                String gsonStr = gson.toJson(list);
                ClientResponse clientResponse = resource.post(gsonStr);   		
                logger.debug("Status code from calculatSum: {}", clientResponse.getStatusCode());
		String gsonResponse = clientResponse.getEntity(String.class);
                
                DtoGoodsDetails sum = gson.fromJson(gsonResponse, DtoGoodsDetails.class);

                return sum;
                
        }
}
