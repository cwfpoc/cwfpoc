/**
 * 
 */
package com.dsv.road.shared.customerorder.dto;

import java.util.ArrayList;

/**
 * @author ext.jesper.munkholm
 *
 */

public class DtoGoods {

    protected DtoGoodsDetails goodsDetails;
    protected ArrayList<DtoGoodsDetails> goodsContainedList;

    
    
    /**
	 * 
	 */
	public DtoGoods() {
		super();
	}

	/**
	 * @param goodsDetails
	 */
	public DtoGoods(DtoGoodsDetails goodsDetails) {
		super();
		this.goodsDetails = goodsDetails;
	}

	/**
     * Gets the value of the goodsDetails property.
     * 
     * @return
     *     possible object is
     *     {@link DtoGoodsDetails }
     *     
     */
    public DtoGoodsDetails getGoodsDetails() {
    	if (goodsDetails == null)
    		goodsDetails = new DtoGoodsDetails();
        return goodsDetails;
    }

    /**
     * Sets the value of the goodsDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link DtoGoodsDetails }
     *     
     */
    public void setGoodsDetails(DtoGoodsDetails value) {
        this.goodsDetails = value;
    }

	/**
	 * @return
	 */
	public ArrayList<DtoGoodsDetails> getGoodsContainedList() {
		if (goodsContainedList == null)
			goodsContainedList = new ArrayList<DtoGoodsDetails>();
		return goodsContainedList;
	}

	/**
	 * @param goodsContainedList
	 */
	public void setGoodsContainedList(ArrayList<DtoGoodsDetails> goodsContainedList) {
		this.goodsContainedList = goodsContainedList;
	}

	public void addGoodsDetails(DtoGoodsDetails goodsDetails) {
		if (goodsContainedList == null) {
			goodsContainedList = new ArrayList<DtoGoodsDetails>();
		}
		
		goodsContainedList.add(goodsDetails);
	}
}

