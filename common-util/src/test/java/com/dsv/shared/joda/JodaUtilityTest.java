package com.dsv.shared.joda;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import junit.framework.TestCase;

public class JodaUtilityTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testGetLocalTimeFromString() {
		LocalTime time = JodaUtility.getLocalTimeFromString("13:24");
		assertEquals("13:24:00.000", time.toString());
	}

	public void testGetLocalDateFromString() {
		LocalDate date = JodaUtility.getLocalDateFromString("2015-04-10");
		assertEquals("2015-04-10", date.toString());
	}

	public void testGetLocalDateTimeFromString() {
		DateTime datetime = JodaUtility.getLocalDateTimeFromString("2015-04-10T13:24");
		assertEquals("2015-04-10T13:24:00.000+02:00", datetime.toString());
	}

	public void testGetLocalTimeAsString() {
		String time = JodaUtility.getLocalTimeAsString(LocalTime.parse("13:24"));
		assertEquals("13:24", time);
	}

	public void testGetLocalDateAsString() {
		String time = JodaUtility.getLocalDateAsString(LocalDate.parse("2015-04-10"));
		assertEquals("2015-04-10", time);
	}

	public void testGetDateTimeAsString() {
		String time = JodaUtility.getDateTimeAsString(DateTime.parse("2015-04-10T13:24:00.000+02:00"));
		assertEquals("2015-04-10T13:24:00.000+02:00", time);
	}

}
