/**
 * 
 */
package com.dsv.shared.dozer;

import java.util.ArrayList;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

/**
 * @author ext.jesper.munkholm
 *
 */
public class DozerUtility {

	public static Mapper getMapper() {
		ArrayList<String> list = new ArrayList<String>();
		list.add("dozer-config.xml");
		Mapper mapper = new DozerBeanMapper(list);
		
		return mapper;
	}
}
