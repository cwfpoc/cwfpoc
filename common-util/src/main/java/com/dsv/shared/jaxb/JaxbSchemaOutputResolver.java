/**
 * 
 */
package com.dsv.shared.jaxb;

import java.io.File;
import java.io.IOException;

import javax.xml.bind.SchemaOutputResolver;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;

/**
 * @author ext.jesper.munkholm
 *
 */
public class JaxbSchemaOutputResolver extends SchemaOutputResolver {

	private File outputFile = null;
	
	public JaxbSchemaOutputResolver(String folder, String filename) {
		outputFile = new File(folder, filename);
	}
	
	@Override
	public Result createOutput(String namespaceUri,
			String suggestedFileName) throws IOException {
		
		return new StreamResult(outputFile);
	}

	
}
