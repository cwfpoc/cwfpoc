/**
 * 
 */
package com.dsv.shared.jaxb;

import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.SchemaOutputResolver;

/**
 * @author ext.jesper.munkholm
 *
 */
public class JaxbSchemaOutput {

	String folder = null;
	String filename = null;
	SchemaOutputResolver resolver = null;
	
	public JaxbSchemaOutput(String folder, String filename) {
		this.folder = folder;
		this.filename = filename;
		resolver = new JaxbSchemaOutputResolver(folder, filename);
	}
	
	public void execute(Class...classes) throws JAXBException, IOException {
		JAXBContext context = JAXBContext.newInstance(classes);
		context.generateSchema(resolver);
	}
}
