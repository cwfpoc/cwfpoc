package com.dsv.shared.ws.rs;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

public class ClientApplication extends Application {
	private Set<Object> singletons;

	public ClientApplication() {
		singletons = new HashSet<Object>();
		singletons.add(new JacksonJaxbJsonProvider());
	}
	
	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}

	public void setSingletons(final Set<Object> singletons) {
		this.singletons = singletons;
	}
}
