/**
 * 
 */
package com.dsv.shared.joda.jaxb;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * @author ext.jesper.munkholm
 *
 */
public class JaxbLocalTimeAdapter extends XmlAdapter<String, LocalTime> {
	
	static DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm");
	
	public LocalTime unmarshal(String v) throws Exception {
        return LocalTime.parse(v, fmt);
    }
 
    public String marshal(LocalTime v) throws Exception {
        return v.toString(fmt);
    }
}
