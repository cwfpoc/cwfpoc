/**
 * 
 */
package com.dsv.shared.joda.gson;

import java.lang.reflect.Type;

import org.joda.time.LocalDate;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * @author ext.jesper.munkholm
 * 
 */
public class JsonLocalDateAdapter implements JsonSerializer<LocalDate>,
		JsonDeserializer<LocalDate> {

	public JsonElement serialize(LocalDate src, Type typeOfSrc,
			JsonSerializationContext context) {
		return new JsonPrimitive(src.toString());
	}

	public LocalDate deserialize(JsonElement json, Type typeOfT,
			JsonDeserializationContext context) throws JsonParseException {
		return new LocalDate(json.getAsJsonPrimitive().getAsString());
	}

}
