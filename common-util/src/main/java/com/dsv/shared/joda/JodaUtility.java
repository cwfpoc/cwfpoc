/**
 * 
 */
package com.dsv.shared.joda;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.dsv.shared.joda.gson.JsonDateTimeAdapter;
import com.dsv.shared.joda.gson.JsonLocalDateAdapter;
import com.dsv.shared.joda.gson.JsonLocalTimeAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author ext.jesper.munkholm
 *
 */
public class JodaUtility {

	static DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm");
	
	public static Gson getGson() {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(LocalTime.class, new JsonLocalTimeAdapter());
		gsonBuilder.registerTypeAdapter(LocalDate.class, new JsonLocalDateAdapter());
		gsonBuilder.registerTypeAdapter(DateTime.class, new JsonDateTimeAdapter());
		
		return gsonBuilder.create();
	}
	
	public static LocalTime getLocalTimeFromString(String localTimeAsString) {
		if (localTimeAsString == null)
			return null;
		return LocalTime.parse(localTimeAsString, fmt);
	}
	
	public static LocalDate getLocalDateFromString(String localDateAsString) {
		if (localDateAsString == null)
			return null;
		return new LocalDate(localDateAsString);
	}
	
	public static DateTime getLocalDateTimeFromString(String dateTimeAsString) {
		if (dateTimeAsString == null)
			return null;
		return new DateTime(dateTimeAsString);
	}
	
	public static String getLocalTimeAsString(LocalTime localTime) {
		return localTime.toString(fmt);
	}
	
	public static String getLocalDateAsString(LocalDate localDate) {
		return localDate.toString();
	}
	
	public static String getDateTimeAsString(DateTime dateTime) {
		return dateTime.toString();
	}
	
}
