/**
 * 
 */
package com.dsv.shared.joda.dozer;

import org.dozer.DozerConverter;
import org.joda.time.LocalDate;

/**
 * @author ext.jesper.munkholm
 *
 */
public class DozerLocalDateAdapter extends DozerConverter<String, LocalDate> {
	public DozerLocalDateAdapter() {
		super(String.class, LocalDate.class);
	}

	@Override
	public String convertFrom(LocalDate localDate, String arg1) {
		return localDate.toString();
	}

	@Override
	public LocalDate convertTo(String localDateAsString, LocalDate arg1) {
		return LocalDate.parse(localDateAsString);
	}
}
