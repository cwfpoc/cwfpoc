/**
 * 
 */
package com.dsv.shared.joda.dozer;

import org.dozer.DozerConverter;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * @author ext.jesper.munkholm
 *
 */
public class DozerLocalTimeAdapter extends DozerConverter<String, LocalTime> {
	
	public DozerLocalTimeAdapter() {
		super(String.class, LocalTime.class);
	}

	static DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm");
	
	@Override
	public String convertFrom(LocalTime localTime, String arg1) {
		return localTime.toString(fmt);
	}

	@Override
	public LocalTime convertTo(String localTimeAsString, LocalTime arg1) {
		return LocalTime.parse(localTimeAsString, fmt);
	}
}
