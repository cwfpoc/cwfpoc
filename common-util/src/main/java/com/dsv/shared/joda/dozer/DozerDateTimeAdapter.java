/**
 * 
 */
package com.dsv.shared.joda.dozer;

import org.dozer.DozerConverter;
import org.joda.time.DateTime;

/**
 * @author ext.jesper.munkholm
 *
 */
public class DozerDateTimeAdapter extends DozerConverter<String, DateTime> {
	public DozerDateTimeAdapter() {
		super(String.class, DateTime.class);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String convertFrom(DateTime dateTime, String arg1) {
		// TODO Auto-generated method stub
		return dateTime.toString();
	}

	@Override
	public DateTime convertTo(String dateTimeAsString, DateTime arg1) {
		return new DateTime(dateTimeAsString);
	}
}
