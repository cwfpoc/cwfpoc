/**
 * 
 */
package com.dsv.shared.joda.gson;

import java.lang.reflect.Type;

import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * @author ext.jesper.munkholm
 * 
 */
public class JsonLocalTimeAdapter implements JsonSerializer<LocalTime>,
		JsonDeserializer<LocalTime> {

	static DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm");

	public JsonElement serialize(LocalTime src, Type typeOfSrc,
			JsonSerializationContext context) {
		return new JsonPrimitive(src.toString(fmt));
	}

	public LocalTime deserialize(JsonElement json, Type typeOfT,
			JsonDeserializationContext context) throws JsonParseException {
		return LocalTime.parse(json.getAsJsonPrimitive().getAsString(), fmt);
	}
}