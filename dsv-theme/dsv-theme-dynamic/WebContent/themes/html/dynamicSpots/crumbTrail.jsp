<%@ page session="false" buffer="none" %> 
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="../includePortalTaglibs.jspf" %>

<%-- lazy load the selection path array --%>
<portal-core:lazy-set var="selectionPath" elExpression="wp.selectionModel.selectionPath"/>

<%-- true if hidden pages should be shown in the navigation --%>
<portal-core:lazy-set var="showHiddenPages" elExpression=="wp.publicRenderParam['{http://www.ibm.com/xmlns/prod/websphere/portal/publicparams}hiddenPages']" />

<%-- Display the breadcrumb when the current page is at least 3 levels away from the navigation content root --%>
<c:if test="${fn:length(selectionPath) > 3}">
	<div class="wpthemeCrumbTrail wpthemeLeft">

		<%-- Loop through the selection path starting at the primary navigation level (2) --%>
		<c:forEach var="node" items="${selectionPath}" begin="2" varStatus="status">

			<%-- Set the current node's ID, and if it is currently selected --%>
			<c:set var="nodeID" value="${wp.identification[node]}"/>
			<c:set var="isSelected" value="${wp.identification[wp.selectionModel.selected] == nodeID}"/>

			<%-- Show the current node if it is not hidden OR if it is in the selection path OR hidden pages are currently being shown --%>
			<c:set var="isHiddenPage" value="${node.metadata['com.ibm.portal.Hidden'] || (isMobile && node.metadata['com.ibm.portal.mobile.Hidden'])}" />
			<c:if test="${!isHiddenPage || showHiddenPages || wp.selectionModel[node]!=null}">

				<%-- Print out a separator before the page title if it is not the first item in the breadcrumb --%>
				<c:if test="${status.count > 1}">
					<span class="wpthemeCrumbTrailSeparator">&gt;</span>
				</c:if>

				<%-- if the node is currently selected, make it bold --%>
				<c:if test="${isSelected}"><strong></c:if>

				<%-- if the node is not currently selected, make the title a link --%>
				<c:if test="${!isSelected}"><a href="?uri=nm:oid:${nodeID}"></c:if>

					<%-- add a CSS class if the node is currently selected --%>
					<span <c:if test="${isSelected}">class="wpthemeSelected" </c:if>lang="${node.title.xmlLocale}" dir="${node.title.direction}">
								
						<c:choose>

							<%-- if the page has a draft in the current project, put the title inside parentheses --%>
							<c:when test="${node.projectID != null}">
								(
								<c:choose>
									<%-- if the page is also hidden, put the title inside brackets as well --%>
									<c:when test="${isHiddenPage}">
										[<c:out value="${node.title}"/>]
									</c:when>
									<%-- otherwise, print out the title within parentheses only --%>
									<c:otherwise>
										<c:out value="${node.title}"/>
									</c:otherwise>
								</c:choose>
								)
							</c:when>

							<%-- if the page is hidden, put the title inside brackets --%>
							<c:when test="${isHiddenPage}">
								[<c:out value="${node.title}"/>]
							</c:when>

							<%-- otherwise, print out the title with no decoration --%>
							<c:otherwise>
								<c:out value="${node.title}"/>
							</c:otherwise>

						</c:choose>

					<%-- close all node title markup --%>
					</span>
				<c:if test="${!isSelected}"></a></c:if>
				<c:if test="${isSelected}"></strong></c:if>

			</c:if>
		</c:forEach>
	</div><%-- close breadcrumb container --%>
</c:if>


