<%@ page session="false" buffer="none" %> 
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="../includePortalTaglibs.jspf" %>

<%-- This JSP prints out a nested side navigation by recursing the navigation tree --%>

<%-- true if hidden pages should be shown in the navigation --%>
<portal-core:lazy-set var="showHiddenPages" elExpression=="wp.publicRenderParam['{http://www.ibm.com/xmlns/prod/websphere/portal/publicparams}hiddenPages']" />

<%-- The children of the parent will be output in the HTML below.
		If this is the first time the JSP is being called in the recursion, 
		parentNode will be empty and should be set to the given start level of the navigation --%>
<c:if test="${empty parentNode}">
	<c:set var="parentNode" value="${wp.selectionModel.selectionPath[param.startLevel]}" />
</c:if>

<%-- open up the list into which sibling pages will be added --%>
<ul class="wpthemeNavList wpthemeNavContainer">

<%-- loop through all the children of the parent node --%>
<c:forEach var="node" items="${wp.navigationModel.children[parentNode]}">

	<%-- display the page if it is not hidden OR hidden pages are being shown --%>
	<c:set var="isHiddenPage" value="${node.metadata['com.ibm.portal.Hidden'] || (isMobile && node.metadata['com.ibm.portal.mobile.Hidden'])}" />
	<c:if test="${!isHiddenPage || showHiddenPages}">
		
		<%-- true if the given node is the currently selected node in the navigation --%>
		<c:set var="isSelectedNode" value="${wp.identification[node] == wp.identification[wp.selectionModel.selected]}"/>

		<%-- set the CSS class to be placed on the page title anchor below --%>
		<c:set var="titleClass" value=""/>
		<%-- if the page has a draft in the current project, choose the wpthemeDraftPageText class --%>
		<c:if test="${node.projectID != null}"><c:set var="titleClass" value=" wpthemeDraftPageText"/></c:if>
		<%-- if the page is hidden, choose the wpthemeHiddenPageText class --%>
		<c:if test="${isHiddenPage}"><c:set var="titleClass" value=" wpthemeHiddenPageText"/></c:if>
		<%-- if the page is BOTH in the current project and hidden, choose the wpthemeHiddenDraftPageText class --%>
		<c:if test="${isHiddenPage && node.projectID != null}"><c:set var="titleClass" value=" wpthemeHiddenDraftPageText"/></c:if>
			
		<%-- begin page markup --%>
		<li class="wpthemeNavListItem">
			<span>

				<%-- output a link to the page, and add a CSS class if it is the currently selected page --%>
				<a href="?uri=nm:oid:${wp.identification[node]}" class="<c:if test="${isSelectedNode}">wpthemeSelected</c:if>${titleClass}"><%--

					--%><%-- start page title markup --%><%--
					--%><span lang="${node.title.xmlLocale}" dir="${node.title.direction}"><%--

						<!-- print out the page title -->
						--%><c:out value="${node.title}"/><%--

						<!-- mark the page if it is currently selected for accessibility -->
						--%><c:if test="${isSelectedNode}"><span class="wpthemeAccess"> <portal-fmt:text key="currently_selected" bundle="nls.commonTheme"/></span></c:if><%--

					--%></span><%-- end page title markup --%><%--
				--%></a>

				<%-- output the close icon for dynamic pages --%>
				<portal-dynamicui:closePage node="${node}">
					<a class="wpthemeClose" href="<%closePageURL.write(out);%>" aria-label="<portal-fmt:text key="theme.close" bundle="nls.commonUI"/>">&#10799;</a>
				</portal-dynamicui:closePage>

			</span>

			<%-- if the given node has children, call this JSP again to print them out recursively --%>
			<c:if test="${wp.navigationModel.hasChildren[node]}">
				<c:set var="parentNode" value="${node}" scope="request"/>
				<jsp:include page="sideNavigation.jsp"/>
			</c:if>
				
		</li><%-- end page markup --%>
			
	</c:if>
</c:forEach>

</ul><%-- close the list of pages --%>
