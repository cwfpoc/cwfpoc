package com.dsv.road.customerorder.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsv.road.customerorder.ejb.GoodsDetailCalculation;
import com.dsv.road.shared.customerorder.dto.DtoGoodsDetails;

/**
 * @author ext.jesper.munkholm
 * 
 *         This implements the Rest full web-service of the CustomerOrder module
 */
@Stateless
@Path("/goods_details")
public class GoodsDetailsService {

	final Logger logger = LoggerFactory.getLogger(GoodsDetailsService.class);

	@Context
	UriInfo uriInfo;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/sum")
	public Response sum(List<DtoGoodsDetails> goodsDetails) {
		if (goodsDetails != null) {
			DtoGoodsDetails sum = GoodsDetailCalculation.SumGoodsDetail(goodsDetails);
			return Response.ok(sum).build();
		}
		return Response.ok(new DtoGoodsDetails()).build();
	}
	

}
