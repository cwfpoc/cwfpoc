package com.dsv.road.customerorder.service;


import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsv.road.customerorder.ejb.CustomerOrderBean;
import com.dsv.road.customerorder.exception.CustomerOrderException;
import com.dsv.road.shared.customerorder.dto.DtoOrderLiteList;

/**
 * @author ext.jesper.munkholm
 * 
 *         This implements the Rest full web-service of the CustomerOrder module
 */
@Stateless
@Path("/orderslite")
public class OrdersLiteResource {

	final Logger logger = LoggerFactory.getLogger(OrdersLiteResource.class);

	@Context
	UriInfo uriInfo;

	@EJB
	CustomerOrderBean coBean;


	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response listAll() throws CustomerOrderException {
		logger.debug("ENTRY: listAll");

			DtoOrderLiteList dtoOrderLiteList = coBean.listAll();
			logger.debug("EXIT: listAll");
			Response response = Response.ok(dtoOrderLiteList).build();
			return response;
		
	}
	
}
