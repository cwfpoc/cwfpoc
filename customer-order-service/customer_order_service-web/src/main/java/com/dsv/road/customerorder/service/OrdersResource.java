package com.dsv.road.customerorder.service;

import java.net.URI;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsv.road.customerorder.ejb.CustomerOrderBean;
import com.dsv.road.customerorder.exception.CustomerOrderException;
import com.dsv.road.shared.customerorder.dto.DtoOrder;

/**
 * @author ext.jesper.munkholm
 * 
 *         This implements the Rest full web-service of the CustomerOrder module
 */
@Stateless
@Path("/orders")
public class OrdersResource {

	final Logger logger = LoggerFactory.getLogger(OrdersResource.class);

	@Context
	UriInfo uriInfo;

	@EJB
	CustomerOrderBean coBean;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response read(@PathParam("id") String id)
			throws CustomerOrderException {
		
		logger.debug("Attempting to fethc order {}", id);
                long idToFetch = 0;
                try {
                    idToFetch = Long.parseLong(id);
                } catch (NumberFormatException e) {
                    return Response.status(Status.NOT_FOUND).build();
                }
		DtoOrder dtoOrder = coBean.read(idToFetch);
		if (dtoOrder == null)
			logger.debug("Could not find order {}", id);
		else {
			logger.debug("Fetched order {}", dtoOrder.getOrderId());
			
			logger.debug("Pickup list size: {}", dtoOrder.getPickupInstructionList().size());
			logger.debug("Deliver list size: {}", dtoOrder.getDeliverInstructionList().size());
		}
		
		if (dtoOrder != null) {
			Response response = Response.ok(dtoOrder).build();
			return response;
		} else {
			return Response.status(Status.NOT_FOUND).build();
		}
	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id)
			throws CustomerOrderException {
		coBean.delete(id);
		Response response = Response.ok().build();
		return response;
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(DtoOrder dtoOrder) throws CustomerOrderException {
			logger.debug("ENTRY: createCustomerOrder");
			try {
				
			
			dtoOrder = coBean.insert(dtoOrder);
			} catch (Exception e) {
				String newString = e.getMessage() + ": Found in create in REST";
				throw new CustomerOrderException(newString);
			}
			
			if (dtoOrder == null) {
				return Response.notModified("The insertion returned null")
						.build();
			}
			UriBuilder ub = uriInfo.getRequestUriBuilder();
			URI orderURI = ub.path(OrdersResource.class, "read").build(
					dtoOrder.getOrderId());

			logger.debug("CustomerOrder created with id: "
					+ dtoOrder.getOrderId());
			return Response.created(orderURI).build();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response update(@PathParam("id") Long id, DtoOrder dtoOrder) {
		logger.debug("ENTRY: createCustomerOrder");

		try {
			dtoOrder = coBean.update(id, dtoOrder);

			if (dtoOrder == null) {
				return Response.notModified().build();
			}
			logger.debug("CustomerOrder updated with id: "
					+ dtoOrder.getOrderId());
			return Response.ok().build();
		} catch (Exception e) {
			return Response.serverError().build();
		}
	}

}
