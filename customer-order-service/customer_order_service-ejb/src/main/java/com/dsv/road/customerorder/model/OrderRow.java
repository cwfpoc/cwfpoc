package com.dsv.road.customerorder.model;

import org.joda.time.DateTime;

import com.dsv.road.customerorder.xml.v1.Order;

/**
 * @author ext.jesper.munkholm
 * 
 */
public class OrderRow {

	private Long id = 0L;
	private DateTime createdAt;
	private String createdBy = "";
	private DateTime updatedAt;
	private String updatedBy = "";
	private Order order = null;

	public OrderRow() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(DateTime createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(DateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}
}
