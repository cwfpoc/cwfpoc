package com.dsv.road.customerorder.model;

import java.util.ArrayList;

import com.dsv.road.customerorder.xml.v1.Goods;

public class GoodsList {
	ArrayList<Goods> list;

	public GoodsList(ArrayList<Goods> list) {
		this.list = list;
	}

	public ArrayList<Goods> getList() {
		return list;
	}

	public void setList(ArrayList<Goods> list) {
		this.list = list;
	}

}
