/**
 * 
 */
package com.dsv.road.customerorder.xml.v1;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author ext.jesper.munkholm
 *
 */
@XmlRootElement(name = "CashOnDelivery", namespace = "com.dsv.road.customerorder.xml.v1")
@XmlAccessorType(XmlAccessType.FIELD)
public class CashOnDelivery {

    @XmlElement(name = "CurrencyCode", required = true)
    protected String currencyCode;
    @XmlElement(name = "Amount", required = true)
    protected BigDecimal amount;

    
    /**
	 * 
	 */
	public CashOnDelivery() {
		super();
	}

	/**
     * @param currencyCode
     * @param amount
     */
    public CashOnDelivery(String currencyCode, BigDecimal amount) {
		super();
		this.currencyCode = currencyCode;
		this.amount = amount;
	}

	/**
     * Gets the value of the amount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

}
