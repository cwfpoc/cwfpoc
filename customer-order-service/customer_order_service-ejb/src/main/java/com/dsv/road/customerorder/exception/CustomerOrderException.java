package com.dsv.road.customerorder.exception;

public class CustomerOrderException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomerOrderException() {
	}

	public CustomerOrderException(String message) {
		super(message);
	}

	public CustomerOrderException(Throwable cause) {
		super(cause);
	}

	public CustomerOrderException(String message, Throwable cause) {
		super(message, cause);
	}


}
