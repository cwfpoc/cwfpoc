package com.dsv.road.customerorder.model;

import java.math.BigDecimal;

public class GoodsDetail {
	
	private Integer numberOfPackages;
    private String packageType;
    private BigDecimal grossWeight;
    private BigDecimal grossVolume;
    private BigDecimal loadingMeters;
    private String description;
    private BigDecimal length;
    private BigDecimal width;
    private BigDecimal height;
	
    public Integer getNumberOfPackages() {
		return numberOfPackages;
	}


	public void setNumberOfPackages(Integer numberOfPackages) {
		this.numberOfPackages = numberOfPackages;
	}


	public String getPackageType() {
		return packageType;
	}


	public void setPackageType(String packageType) {
		this.packageType = packageType;
	}


	public BigDecimal getGrossWeight() {
		return grossWeight;
	}


	public void setGrossWeight(BigDecimal grossWeight) {
		this.grossWeight = grossWeight;
	}


	public BigDecimal getGrossVolume() {
		return grossVolume;
	}


	public void setGrossVolume(BigDecimal grossVolume) {
		this.grossVolume = grossVolume;
	}


	public BigDecimal getLoadingMeters() {
		return loadingMeters;
	}


	public void setLoadingMeters(BigDecimal loadingMeters) {
		this.loadingMeters = loadingMeters;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public BigDecimal getLength() {
		return length;
	}


	public void setLength(BigDecimal length) {
		this.length = length;
	}


	public BigDecimal getWidth() {
		return width;
	}


	public void setWidth(BigDecimal width) {
		this.width = width;
	}


	public BigDecimal getHeight() {
		return height;
	}


	public void setHeight(BigDecimal height) {
		this.height = height;
	}

    // Length (m) x Width (m) x Height (m) = Cubic meter (m3)
    public BigDecimal getCubicMetres() {
		return length.multiply(width).multiply(height);
	}

}
