package com.dsv.road.customerorder.ejb;

import java.util.List;

import com.dsv.road.shared.customerorder.dto.DtoGoodsDetails;

public class GoodsDetailCalculation {
	
	private static final String DEFAULT_PACKAGE_TYPE = "CLL";

	public static DtoGoodsDetails SumGoodsDetail(List<DtoGoodsDetails> goodsDetails) {
		DtoGoodsDetails sum = null;
		for(DtoGoodsDetails goodsDetail: goodsDetails) {
			if (sum == null) {
				sum = goodsDetail;
			} else {
				sum = AddGoodsDeetails(sum, goodsDetail);
			}
		}
		return sum;
	}

	public static DtoGoodsDetails AddGoodsDeetails(DtoGoodsDetails goodsDetail1, DtoGoodsDetails goodsDetail2) {
		DtoGoodsDetails sum = new DtoGoodsDetails();

		if (goodsDetail1.getPackageType().equals(goodsDetail2.getPackageType())) {
			sum.setPackageType(goodsDetail1.getPackageType());
		}
		else {
			sum.setPackageType(DEFAULT_PACKAGE_TYPE);
		}
		
		sum.setNumberOfPackages(goodsDetail1.getNumberOfPackages() + goodsDetail2.getNumberOfPackages());
		sum.setGrossWeight(goodsDetail1.getGrossWeight().add(goodsDetail2.getGrossWeight()));
		sum.setGrossVolume(goodsDetail1.getGrossVolume().add(goodsDetail2.getGrossVolume()));
		sum.setLoadingMeters(goodsDetail1.getLoadingMeters().add(goodsDetail2.getLoadingMeters()));

		return sum;
	}

}
