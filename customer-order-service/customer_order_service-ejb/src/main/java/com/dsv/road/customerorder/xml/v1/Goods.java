/**
 * 
 */
package com.dsv.road.customerorder.xml.v1;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author ext.jesper.munkholm
 *
 */

@XmlRootElement(name = "Goods", namespace = "com.dsv.road.customerorder.xml.v1")
@XmlAccessorType(XmlAccessType.FIELD)
public class Goods {

    @XmlElement(name = "GoodsDetails", required = true)
    protected GoodsDetails goodsDetails;
	@XmlElementWrapper(name = "GoodsContainedList")
    @XmlElement(name = "GoodsContained")
    protected ArrayList<GoodsDetails> goodsContainedList;

    
    
    /**
	 * 
	 */
	public Goods() {
		super();
	}

	/**
	 * @param goodsDetails
	 */
	public Goods(GoodsDetails goodsDetails) {
		super();
		this.goodsDetails = goodsDetails;
	}

	/**
     * Gets the value of the goodsDetails property.
     * 
     * @return
     *     possible object is
     *     {@link GoodsDetails }
     *     
     */
    public GoodsDetails getGoodsDetails() {
        return goodsDetails;
    }

    /**
     * Sets the value of the goodsDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link GoodsDetails }
     *     
     */
    public void setGoodsDetails(GoodsDetails value) {
        this.goodsDetails = value;
    }

	/**
	 * @return
	 */
	public ArrayList<GoodsDetails> getGoodsContainedList() {
		return goodsContainedList;
	}

	/**
	 * @param goodsContainedList
	 */
	public void setGoodsContainedList(ArrayList<GoodsDetails> goodsContainedList) {
		this.goodsContainedList = goodsContainedList;
	}

	public void addGoodsDetails(GoodsDetails goodsDetails) {
		if (goodsContainedList == null) {
			goodsContainedList = new ArrayList<GoodsDetails>();
		}
		
		goodsContainedList.add(goodsDetails);
	}
}

