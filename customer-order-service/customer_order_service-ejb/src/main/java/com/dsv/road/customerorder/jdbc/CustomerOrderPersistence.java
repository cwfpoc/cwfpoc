package com.dsv.road.customerorder.jdbc;

import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsv.road.customerorder.exception.CustomerOrderException;
import com.dsv.road.customerorder.model.OrderRow;
import com.dsv.road.customerorder.xml.v1.Goods;
import com.dsv.road.customerorder.xml.v1.Order;

@Stateless
public class CustomerOrderPersistence {

	final Logger logger = LoggerFactory
			.getLogger(CustomerOrderPersistence.class);

	private Connection dbConn = null;
	private PreparedStatement psSelectCustomerOrder = null;
	private PreparedStatement psInsertCustomerOrder = null;
	private PreparedStatement psUpdateCustomerOrder = null;
	private PreparedStatement psDeleteCustomerOrder = null;

	private JAXBContext jContext = null;
	private Unmarshaller unmarshaller = null;
	private Marshaller marshaller = null;

	private DataSource ds = null;
	// should be wepsphere variable?
	private static String DS_JNDI_NAME = "jdbc/dsvDS";

	/**
	 * Constructor used when the connection is injected
	 */
	public CustomerOrderPersistence() {

	}

	/**
	 * Constructor used when the connection is provided externally
	 * 
	 * @param dbConn
	 *            is the database base connection to use
	 */
	public CustomerOrderPersistence(Connection dbConn) {

		this.dbConn = dbConn;

	}

	private void fetchConnectionFromWAS() throws CustomerOrderException {
		logger.info("ENTRY: fetchConnectionFromWAS");
		Context ctx;
		try {
			if (ds == null) {
				ctx = new InitialContext();
				ds = (DataSource) ctx.lookup(DS_JNDI_NAME);
			}
			dbConn = ds.getConnection();
		} catch (NamingException e) {
			throw new CustomerOrderException("Unable to find JNDI name "
					+ DS_JNDI_NAME + ": " + e.getMessage());
		} catch (SQLException e) {
			throw new CustomerOrderException("No connection from datasource: "
					+ e.getMessage());
		}

		if (dbConn == null) {
			throw new CustomerOrderException(
					"Unable to get access to the database via JNDI name: "
							+ DS_JNDI_NAME);
		}
		logger.info("EXIT: fetchConnectionFromWAS");
	}

	private PreparedStatement getPrepairedStatementForSelectCustomerOrder()
			throws SQLException {
		String sql = "SELECT ID,CREATED_AT, CREATED_BY,UPDATED_AT,UPDATED_BY, ORDER_XML " + "FROM ODB.CUSTOMER_ORDER "
				+ "WHERE ID = ? and DELETED IS NULL";
		return dbConn.prepareStatement(sql);
	}

	private PreparedStatement getPrepairedStatementForUpdateCustomerOrder()
			throws SQLException {
		String sql = "SELECT ID,CREATED_AT, CREATED_BY,UPDATED_AT,UPDATED_BY, ORDER_XML FROM NEW TABLE(UPDATE ODB.CUSTOMER_ORDER "
				+ "SET UPDATED_AT=CURRENT_DATE,UPDATED_BY=?, ORDER_XML=? "
				+ "WHERE ID = ?)";
		return dbConn.prepareStatement(sql);
	}

	private PreparedStatement getPrepairedStatementForInsertCustomerOrder()
			throws SQLException {
		String sql = "SELECT ID FROM NEW TABLE(INSERT INTO ODB.CUSTOMER_ORDER (ID, CREATED_AT, CREATED_BY,XSD, ORDER_XML) VALUES (DEFAULT, CURRENT_TIMESTAMP, ?,DEFAULT, ?))";
		return dbConn.prepareStatement(sql);
	}

	private PreparedStatement getPrepairedStatementForDeleteCustomerOrder()
			throws SQLException {
		String sql = "UPDATE ODB.CUSTOMER_ORDER SET DELETED = CURRENT_TIMESTAMP WHERE ID = ?";
		return dbConn.prepareStatement(sql);
	}

	// to ensure no race condition occurs when the system i started
	private synchronized void initialize() throws CustomerOrderException {
		logger.info("ENTRY: prepareDatabaseAccess");

		// if the connection has not been supplied from else where then check
		// for a datasource
		if (dbConn == null) {
			fetchConnectionFromWAS();
		}

		try {
			psSelectCustomerOrder = getPrepairedStatementForSelectCustomerOrder();

			psInsertCustomerOrder = getPrepairedStatementForInsertCustomerOrder();

			psUpdateCustomerOrder = getPrepairedStatementForUpdateCustomerOrder();

			psDeleteCustomerOrder = getPrepairedStatementForDeleteCustomerOrder();

		} catch (SQLException e) {
			throw new CustomerOrderException(
					"Unable to prepared select for CustomerOrder: "
							+ e.getMessage());
		}
		initializeMarshallers();

		logger.info("EXIT: prepareDatabaseAccess");

		if (marshaller == null)
			logger.error("The marshaller is not set");
	}

	private void initializeMarshallers() throws CustomerOrderException {
		try {
			jContext = JAXBContext.newInstance(Order.class);
			unmarshaller = jContext.createUnmarshaller();
			jContext = JAXBContext.newInstance(Order.class);
			marshaller = jContext.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
					Boolean.TRUE);
		} catch (JAXBException e) {
			throw new CustomerOrderException(
					"Unable to initialize JAXB marshaller and unmarshaller: "
							+ e.getMessage());

		}
	}

	public OrderRow insertOrder(Order order) throws CustomerOrderException {

		if (marshaller == null) {
			initializeMarshallers();
		}
		if (dbConn == null) {
			setConnection();
		}
		OrderRow orderRow = new OrderRow();
		try {
			psInsertCustomerOrder = getPrepairedStatementForInsertCustomerOrder();
			StringWriter sw = new StringWriter();
			marshaller.marshal(order, sw);
			psInsertCustomerOrder.setString(1, "NotDefined");
			psInsertCustomerOrder.setString(2, sw.toString());

			ResultSet rs = psInsertCustomerOrder.executeQuery();
			if (rs.next()) {
				orderRow.setId(rs.getLong(1));
				order.setOrderId(String.valueOf(orderRow.getId()));
				orderRow = updateOrder(orderRow.getId(),order);
				rs.close();
				return orderRow;
			} else {
				throw new CustomerOrderException("Insert customer went wrong.");
			}
		} catch (SQLException e) {
			throw new CustomerOrderException(
					"Error occured while reading customer information from database: "
							+ e.getMessage());
		} catch (JAXBException e) {
			throw new CustomerOrderException(
					"Error occured while serializing object to XML: "
							+ e.getMessage());
		}

	}

	public void deleteOrder(long id) throws CustomerOrderException {

		if (marshaller == null) {
			initializeMarshallers();
		}
		if (dbConn == null) {
			setConnection();
		}

		try {
			psDeleteCustomerOrder = getPrepairedStatementForDeleteCustomerOrder();

			psDeleteCustomerOrder.setLong(1, id);
			int result = psDeleteCustomerOrder.executeUpdate();
			if (result != 1) {
				throw new CustomerOrderException(
						"Error occured while reading deleting customerOrder, Id was not unique");
			}
		} catch (SQLException e) {
			throw new CustomerOrderException(
					"Error occured while deleting customerOrder from database: "
							+ e.getMessage());
		}

	}

	public OrderRow updateOrder(Long id,Order order) throws CustomerOrderException {

		if (marshaller == null) {
			initializeMarshallers();
		}
		if (dbConn == null) {
			setConnection();
		}
		OrderRow orderRow = new OrderRow();
		try {
			StringWriter sw = new StringWriter();
			marshaller.marshal(order, sw);
			psUpdateCustomerOrder = getPrepairedStatementForUpdateCustomerOrder();
			psUpdateCustomerOrder.setString(1, "NotDefined");
			psUpdateCustomerOrder.setString(2, sw.toString());
			psUpdateCustomerOrder.setLong(3, id);
			ResultSet rs = psUpdateCustomerOrder.executeQuery();
			if (rs.next()) {
				orderRow.setId(rs.getLong(1));
				order.setOrderId(String.valueOf(orderRow.getId()));
				Timestamp createdAt = rs.getTimestamp(2);
				if (createdAt != null){
					orderRow.setCreatedAt(new DateTime(createdAt,DateTimeZone.UTC));
				}
				orderRow.setCreatedBy(rs.getString(3));
				Timestamp updatedAt = rs.getTimestamp(4);
				if (updatedAt != null){
					orderRow.setUpdatedAt(new DateTime(updatedAt,DateTimeZone.UTC));
				}
				orderRow.setUpdatedBy(rs.getString(5));	
				orderRow.setOrder(order);
				rs.close();
			}
		} catch (SQLException e) {
			throw new CustomerOrderException(
					"Error occured while reading customer information from database: "
							+ e.getMessage());
		} catch (JAXBException e) {
			throw new CustomerOrderException(
					"Error occured while serializing object to XML: "
							+ e.getMessage());
		}

		return orderRow;
	}

	public OrderRow getCustomerOrder(long id) throws CustomerOrderException {
		logger.info("ENTRY: getCustomerOrder({})", id);

		if (unmarshaller == null) {
			initializeMarshallers();
		}
		if (dbConn == null) {
			setConnection();
		}
		OrderRow orderRow = new OrderRow();
		try {
			psSelectCustomerOrder = getPrepairedStatementForSelectCustomerOrder();
			psSelectCustomerOrder.setLong(1, id); //TODO: XPATH
			ResultSet rs = psSelectCustomerOrder.executeQuery();
			if (rs.next()) {
				orderRow.setId(rs.getLong(1));
				Timestamp createdAt = rs.getTimestamp(2);
				if (createdAt != null){
					orderRow.setCreatedAt(new DateTime(createdAt,DateTimeZone.UTC));
				}
				orderRow.setCreatedBy(rs.getString(3));
				Timestamp updatedAt = rs.getTimestamp(4);
				if (updatedAt != null){
					orderRow.setUpdatedAt(new DateTime(updatedAt,DateTimeZone.UTC));
				}
				orderRow.setUpdatedBy(rs.getString(5));				
				orderRow.setOrder((Order) unmarshaller.unmarshal(rs.getBinaryStream(6)));
				rs.close();
			}
		} catch (SQLException e) {
			throw new CustomerOrderException(
					"Error occured while reading customer information from database"
							+ e);
		} catch (JAXBException e) {
			throw new CustomerOrderException(
					"Error occured while serializing object to XML: "
							+ e.getMessage());
		}

		logger.info("EXIT: getCustomerOrder(rowid {},  "
				+ (orderRow.getOrder() != null ? orderRow.getOrder().getOrderId() : "null") + ")", orderRow.getId());

		return orderRow;
	}

	public void destroy() {

	}

	private void setConnection() {

		try {
			fetchConnectionFromWAS();
		} catch (CustomerOrderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public ArrayList<Goods> listGoods(Long id) throws CustomerOrderException {
		return getCustomerOrder(id).getOrder().getGoodsList();
	}
}
