package com.dsv.road.customerorder.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.dsv.road.customerorder.exception.CustomerOrderException;
import com.dsv.road.customerorder.model.OrderLite;

@Stateless
public class CustomerOrderInfoPersistence {

	private Connection dbConn = null;
	PreparedStatement psSelectCustomerOrderInfo = null;
	DateTimeFormatter dtf = null;

	private DataSource ds = null;
	// should be wepsphere variable?
	private static String DS_JNDI_NAME = "jdbc/dsvDS";

	/**
	 * Constructor used when the connection is injected
	 */
	public CustomerOrderInfoPersistence() {

	}

	/**
	 * Constructor used when the connection is provided externally
	 * 
	 * @param dbConn
	 *            is the database base connection to use
	 */
	public CustomerOrderInfoPersistence(Connection dbConn) {
		this.dbConn = dbConn;
	}

	private void fetchConnectionFromWAS() throws CustomerOrderException {

		Context ctx;
		try {
			if (ds == null) {
				ctx = new InitialContext();
				ds = (DataSource) ctx.lookup(DS_JNDI_NAME);
			}
			dbConn = ds.getConnection();
		} catch (NamingException e) {
			throw new CustomerOrderException("Unable to find JNDI name "
					+ DS_JNDI_NAME + ": " + e.getMessage());
		} catch (SQLException e) {
			throw new CustomerOrderException("No connection from datasource: "
					+ e.getMessage());
		}

		if (dbConn == null) {
			throw new CustomerOrderException(
					"Unable to get access to the database via JNDI name: "
							+ DS_JNDI_NAME);
		}

	}

	public synchronized void initialize() throws CustomerOrderException {

		// if the connection has not been supplied from else where then check
		// for a datasource
		if (dbConn == null) {
			fetchConnectionFromWAS();
		}
		String sql = "SELECT id, X.* "
				+ "FROM ODB.CUSTOMER_ORDER, "
				+ "XMLTABLE ( "
				+ "'$ORDER_XML/*:Order' passing ORDER_XML as \"ORDER_XML\" "
				+ "COLUMNS "
				+ "orderId            VARCHAR(64)  DEFAULT ''    PATH 'OrderId', "
				+ "shipperPartyName   VARCHAR(100) DEFAULT ''    PATH 'ShipperParty/Name/Name1', "
				+ "consigneePartyName VARCHAR(100) DEFAULT ''    PATH 'ConsigneeParty/Name/Name1', "
				+ "pickupDate         VARCHAR(12)  DEFAULT ''    PATH 'PickupDate/Date', "
				+ "deliverDate        VARCHAR(12)  DEFAULT ''    PATH 'DeliverDate/Date', "
				+ "noOfPackages       VARCHAR(12)  DEFAULT ''    PATH 'GoodsSummary/NumberOfPackages', "
				+ "packageType        VARCHAR(12)  DEFAULT ''    PATH 'GoodsSummary/PackageType' "
				+ ") AS X ";

		try {
			psSelectCustomerOrderInfo = dbConn.prepareStatement(sql);
		} catch (SQLException e) {
			throw new CustomerOrderException(
					"Unable to prepared select for CustomerOrderInfo: "
							+ e.getMessage());
		}

		// ready parser for CreationDateTime
		// 2015-02-12T00:00:00
		dtf = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss").withZone(
				DateTimeZone.UTC);
	}

	public ArrayList<OrderLite> listCustomerOrders()
			throws CustomerOrderException {
		ArrayList<OrderLite> list = new ArrayList<OrderLite>();

		initialize();

		try {
			ResultSet rs = psSelectCustomerOrderInfo.executeQuery();
			while (rs.next()) {
				OrderLite coi = new OrderLite();
				coi.setOrderId(String.valueOf(rs.getLong(1)));
				coi.setShipperPartyName(rs.getString(3));
				coi.setConsigneePartyName(rs.getString(4));
				coi.setPickupDate(rs.getString(4));
				coi.setDeliverDate(rs.getString(5));
				coi.setGoodsSummary(rs.getString(6) + " packages of type: "
						+ rs.getString(7));
				list.add(coi);
			}
		} catch (SQLException e) {
			throw new CustomerOrderException(
					"Error occured while reading customer information from database"
							+ e);
		}
		return list;
	}

	public void destroy() {

	}
}
