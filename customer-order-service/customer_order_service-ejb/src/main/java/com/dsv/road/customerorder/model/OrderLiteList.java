package com.dsv.road.customerorder.model;

import java.util.ArrayList;

public class OrderLiteList {

	private ArrayList<OrderLite> list;
	
	public OrderLiteList(ArrayList<OrderLite> list){
		this.list = list;
	}
	
	
	public ArrayList<OrderLite> getList() {
		return list;
	}

	public void setList(ArrayList<OrderLite> list) {
		this.list = list;
	}

	
}
