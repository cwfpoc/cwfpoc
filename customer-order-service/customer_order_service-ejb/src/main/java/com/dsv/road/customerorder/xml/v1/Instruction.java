/**
 * 
 */
package com.dsv.road.customerorder.xml.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author ext.jesper.munkholm
 * 
 */
@XmlRootElement(name = "Instruction", namespace = "com.dsv.road.customerorder.xml.v1")
@XmlAccessorType(XmlAccessType.FIELD)
public class Instruction {
	
	@XmlElement(name = "Instruction")
	private String value;

	public Instruction() {
		
	}
	
	public Instruction(String value) {
		this.value = value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
