package com.dsv.road.customerorder.exception;

import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;


@Provider
public class CustomerOrderExceptionHandler  implements ExceptionMapper<CustomerOrderException>{

    @Override
    public Response toResponse(CustomerOrderException exception)
    {
        return Response.status(Status.BAD_REQUEST).entity(exception.getMessage()).build(); 
    }
}
