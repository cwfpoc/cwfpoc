/**
 * 
 */
package com.dsv.road.customerorder.xml.v1;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import javax.xml.bind.JAXBException;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import com.dsv.shared.jaxb.JaxbSchemaOutput;

/**
 * @author ext.jesper.munkholm
 * 
 */
public class CreateExamples {

	/**
	 * @param args
	 * @throws JAXBException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws JAXBException, IOException {

		for (int i = 0; i < 100; i++) {
			Order o = new Order(String.valueOf(i));
			
			o.addReference(new Reference("External id", "external id " + i));
			o.addReference(new Reference("Dinas id", "Dinas id " + i));
			
			o.setConsigneeParty(new Party(1, new Name("ConsigneeParty on order " + i)));
			o.getConsigneeParty().setAddress(new Address("DK", "Consignee address"));
			o.setDeliverParty(new Party(2, new Name("DeliverParty on order " + i)));
			o.getDeliverParty().setAddress(new Address("DK", "Delivery address"));
			o.setPickupParty(new Party(3, new Name("PickupParty on order " + i)));
			o.getPickupParty().setAddress(new Address("DK", "Pickup address"));
			o.setShipperParty(new Party(4, new Name("ShipperParty on order " + i)));
			o.getShipperParty().setAddress(new Address("DK", "Shipper address"));
			
			
			GoodsDetails gd = new GoodsDetails(1000, "Pallets", "Test delivery");
			gd.setGrossVolume(new BigDecimal(100));
			gd.setGrossWeight(new BigDecimal(50));
			gd.setHeight(new BigDecimal(1000));
			gd.setWidth(new BigDecimal(200));
			gd.setLoadingMeters(new BigDecimal(1));
			Goods g = new Goods(gd);

			gd = new GoodsDetails(500, "Pallets", "Vacum cleaners");
			gd.setGrossVolume(new BigDecimal(50));
			gd.setGrossWeight(new BigDecimal(25));
			gd.setHeight(new BigDecimal(500));
			gd.setWidth(new BigDecimal(200));
			gd.setLoadingMeters(new BigDecimal(0.5));
			g.addGoodsDetails(gd);
			gd = new GoodsDetails(500, "Pallets", "Pipes");
			gd.setGrossVolume(new BigDecimal(50));
			gd.setGrossWeight(new BigDecimal(25));
			gd.setHeight(new BigDecimal(500));
			gd.setWidth(new BigDecimal(200));
			gd.setLoadingMeters(new BigDecimal(0.5));
			g.addGoodsDetails(gd);
			o.addGoods(g);
			gd = new GoodsDetails(1000, "Pallets", "Summary");
			gd.setGrossVolume(new BigDecimal(100));
			gd.setGrossWeight(new BigDecimal(50));
			gd.setHeight(new BigDecimal(1000));
			gd.setWidth(new BigDecimal(200));
			gd.setLoadingMeters(new BigDecimal(1));
			o.setGoodsSummary(gd);
			
			o.setCashOnDelivery(new CashOnDelivery("DKK", new BigDecimal(1000)));
			o.setTermsOfDelivery(new TermsOfDelivery("Shipper", "Reponsible until delivery"));
			
			o.setPickupDate(new DateType(new LocalDate(2015,4,1), new LocalTime(8,0), new LocalTime(16,0)));
			o.setDeliverDate(new DateType(new LocalDate(2015,5,1), new LocalTime(8,0), new LocalTime(16,0)));
			
			o.addDeliverInstruction(new Instruction("Call 12345678 30 minutes before"));
			o.addDeliverInstruction(new Instruction("Call shipper after to confirm cash on delivery payment"));
			
			o.addPickupInstruction(new Instruction("Peter knows where the goods are. Call him at 12345678"));
			o.addPickupInstruction(new Instruction("Incase Peter is ill, call Hans at 87654321"));
			
			File folder = new File("testdata");
			if (!folder.exists())
				folder.mkdirs();
			File testFile = new File("testdata/CustomerOrder" + i + ".xml");
			Order.saveToFile(o, testFile);
			
		}
		

		Class[] classes = {Order.class,Address.class, CashOnDelivery.class,DateType.class,Goods.class,GoodsDetails.class,
							Name.class,Party.class,Reference.class,TermsOfDelivery.class};
		Class[] classes2 = {Order.class};
		JaxbSchemaOutput schemaOutput = new JaxbSchemaOutput("testdata", "CustomerOrder_V1.xsd");
		schemaOutput.execute(classes2);
	}

}
