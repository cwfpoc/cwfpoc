//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.03.23 at 10:14:04 PM CET 
//


package com.dsv.road.customerorder.xml.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import com.dsv.shared.joda.jaxb.JaxbLocalDateAdapter;
import com.dsv.shared.joda.jaxb.JaxbLocalTimeAdapter;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

@XmlRootElement(name = "DateType", namespace = "com.dsv.road.customerorder.xml.v1")
@XmlAccessorType(XmlAccessType.FIELD)
public class DateType {

    @XmlElement(name = "Date", required = true)
    @XmlJavaTypeAdapter(JaxbLocalDateAdapter.class)
    protected LocalDate date;
    @XmlElement(name = "TimeFrom")
    @XmlJavaTypeAdapter(JaxbLocalTimeAdapter.class)
    protected LocalTime timeFrom;
    @XmlElement(name = "TimeTo")
    @XmlJavaTypeAdapter(JaxbLocalTimeAdapter.class)
    protected LocalTime timeTo;
    
    
	/**
	 * 
	 */
	public DateType() {
		super();
	}

	/**
	 * @param date
	 */
	public DateType(LocalDate date) {
		super();
		this.date = date;
	}

	/**
	 * @param date
	 * @param timeFrom
	 * @param timeTo
	 */
	public DateType(LocalDate date, LocalTime timeFrom, LocalTime timeTo) {
		super();
		this.date = date;
		this.timeFrom = timeFrom;
		this.timeTo = timeTo;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public LocalTime getTimeFrom() {
		return timeFrom;
	}

	public void setTimeFrom(LocalTime timeFrom) {
		this.timeFrom = timeFrom;
	}

	public LocalTime getTimeTo() {
		return timeTo;
	}

	public void setTimeTo(LocalTime timeTo) {
		this.timeTo = timeTo;
	}


}
