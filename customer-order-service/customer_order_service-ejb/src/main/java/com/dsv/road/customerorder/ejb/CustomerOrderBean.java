package com.dsv.road.customerorder.ejb;

import java.util.ArrayList;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.dozer.Mapper;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsv.road.customerorder.exception.CustomerOrderException;
import com.dsv.road.customerorder.jdbc.CustomerOrderInfoPersistence;
import com.dsv.road.customerorder.jdbc.CustomerOrderPersistence;
import com.dsv.road.customerorder.model.GoodsList;
import com.dsv.road.customerorder.model.OrderLite;
import com.dsv.road.customerorder.model.OrderLiteList;
import com.dsv.road.customerorder.model.OrderRow;
import com.dsv.road.customerorder.xml.v1.Goods;
import com.dsv.road.customerorder.xml.v1.Order;
import com.dsv.road.shared.customerorder.dto.DtoGoodsList;
import com.dsv.road.shared.customerorder.dto.DtoOrder;
import com.dsv.road.shared.customerorder.dto.DtoOrderLiteList;
import com.dsv.shared.dozer.DozerUtility;

/**
 * @author ext.jesper.munkholm
 * 
 *         This implements the business layer of the customer order module
 */
@Stateless
public class CustomerOrderBean {

	final Logger logger = LoggerFactory.getLogger(CustomerOrderBean.class);
	DateTimeFormatter dtf = null;

	@EJB
	CustomerOrderPersistence codw;

	@EJB
	CustomerOrderInfoPersistence coidw;

	Mapper mapper = DozerUtility.getMapper();

	public CustomerOrderBean() {
		dtf = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss").withZone(
				DateTimeZone.UTC);
		
	}

	public DtoOrder read(Long id) throws CustomerOrderException {
		OrderRow orderRow = codw.getCustomerOrder(id);
		if (orderRow == null || orderRow.getOrder() == null) {
			return null;
		}
		return mapper.map(orderRow.getOrder(), DtoOrder.class); 
	}

	public void delete(Long id) throws CustomerOrderException {
		codw.deleteOrder(id);
	}

	public DtoOrder update(long id, DtoOrder dtoOrder)
			throws CustomerOrderException {
		if (dtoOrder == null) {
			return null;
		}
		Order order = mapper.map(dtoOrder, Order.class);
		OrderRow orderRow = codw.updateOrder(id, order);
		if (orderRow == null || orderRow.getOrder() == null) {
			return null;
		}
		return mapper.map(orderRow.getOrder(), DtoOrder.class);
	}

	public DtoOrder insert(DtoOrder dtoOrder) throws CustomerOrderException {
		if (dtoOrder == null) {
			return null;
		}
		
		Order order;
		try {

			order = mapper.map(dtoOrder, Order.class);
		} catch (Exception e) {
			e.printStackTrace();

			throw e;
		}
		OrderRow orderRow = codw.insertOrder(order);
		if (orderRow == null || orderRow.getOrder() == null) {
			return null;
		}
		return mapper.map(orderRow.getOrder(), DtoOrder.class);
	}

	public DtoOrderLiteList listAll() throws CustomerOrderException {
		// fetch from customer order in persistence
		ArrayList<OrderLite> customerOrderLiteList = coidw.listCustomerOrders();
		if (customerOrderLiteList == null || customerOrderLiteList.isEmpty()) {
			return new DtoOrderLiteList();
		}
		OrderLiteList orderLiteList = new OrderLiteList(customerOrderLiteList);
		return mapper.map(orderLiteList, DtoOrderLiteList.class);
	}

	public DtoGoodsList getGoodsList(Long id) throws CustomerOrderException {
		ArrayList<Goods> goodsArrayList = codw.listGoods(id);
		if (goodsArrayList == null) {
			return null;
		}
		GoodsList goodsList = new GoodsList(goodsArrayList);
		return mapper.map(goodsList, DtoGoodsList.class);
	}
}
