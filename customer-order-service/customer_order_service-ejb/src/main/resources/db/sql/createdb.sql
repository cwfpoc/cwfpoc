
-- Create the CWFODB
-- as DB2admin run 'db2 -tvf createdb.sql
--
CREATE DATABASE CWFODB AUTOMATIC STORAGE YES USING CODESET UTF-8 TERRITORY DK;
CONNECT TO CWFODB;
--
-- config statements here
--
CONNECT RESET;
