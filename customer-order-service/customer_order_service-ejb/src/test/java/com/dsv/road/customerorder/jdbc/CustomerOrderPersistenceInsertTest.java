package com.dsv.road.customerorder.jdbc;

import static org.junit.Assert.assertNotNull;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsv.road.customerorder.exception.CustomerOrderException;
import com.dsv.road.customerorder.model.OrderLite;
import com.dsv.road.customerorder.model.OrderRow;
import com.dsv.road.customerorder.xml.v1.Order;

public class CustomerOrderPersistenceInsertTest {

	DB2Connection db2 = new DB2Connection();
	CustomerOrderInfoPersistence coiDb = null;
	CustomerOrderPersistence coDb = null;
	ArrayList<OrderLite> list = null;
	Order order = null;
	final Logger logger = LoggerFactory
			.getLogger(CustomerOrderPersistenceInsertTest.class);
	private String testName = "TestName";

	public CustomerOrderPersistenceInsertTest() {

	}

	// in normal cases the connnection is provided by the WAS container, but in
	// this case we create it explicitly
	public boolean setupConnection() {
		try {
			// Establish connection
			PreparedStatement ps = db2
					.prepareStatement("SELECT COUNT(*) FROM ODB.CUSTOMER_ORDER");
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				logger.info("Found {} existing rows in the database",
						rs.getInt(1));
			} else {
				logger.error("Table did not exists");
				return false;
			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	@Before
	public void before() {
		logger.info("Setup for the test");
		db2.openConnection();
		setupConnection();
		coiDb = new CustomerOrderInfoPersistence(db2.getConnection());
		coDb = new CustomerOrderPersistence(db2.getConnection());
	}

	@After
	public void after() {
		db2.rollbackChanges();
		db2.closeConnection();
	}

	@Test
	public void testInsert() {
		logger.info("Running the test");

		logger.info("Checking the connection to the database");
		assertNotNull(db2.getConnection());

		logger.info("Creating new object");
		CustomerOrderInstance ci = new CustomerOrderInstance();
		Order order = ci.getOrderWithShipperName(testName);

		try {
			logger.info("Inserting object");
			OrderRow updatedOrderRow = coDb.insertOrder(order);
			Assert.assertNotNull(updatedOrderRow);

			Order updatedOrder = updatedOrderRow.getOrder();
			Assert.assertNotNull(updatedOrder);
			Assert.assertTrue(Long.valueOf(updatedOrder.getOrderId()) > 0);

			logger.info("Retrieving object from DB");
			OrderRow orderRowFromDB = coDb.getCustomerOrder(Long
					.valueOf(updatedOrder.getOrderId()));
			Assert.assertNotNull(orderRowFromDB);
			Order orderFromDB = orderRowFromDB.getOrder();

			Assert.assertEquals(updatedOrder.getOrderId(),
					orderFromDB.getOrderId());
			Assert.assertTrue(orderFromDB.getShipperParty().getName()
					.getName1().equals(testName));

		} catch (CustomerOrderException e) {
			// TODO Auto-generated catch block
			Assert.fail("Unit test for inserting Order cannot insert new costumerOrder");

			e.printStackTrace();

		}
	}
}
