package com.dsv.road.customerorder.jdbc;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DB2Connection {
	
    private Connection dbConn = null;
    private String url = "jdbc:db2://localhost:50000/CWFODB";
    private String user = "db2admin";
    private String password = "Zaq12wsx";

    final Logger logger = LoggerFactory.getLogger(DB2Connection.class);

    public DB2Connection(){
    	try {
			Properties prop = ReadConfig();
			url = prop.getProperty("databasse_url", url);
			user = prop.getProperty("database_user", user);
			password = prop.getProperty("database_password", password);
			logger.info("Updated database config url: {}, user: {}, password: {}", url, user, password);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
    }
    public Connection getConnection() {
    	
        return dbConn;
    }
    
    public boolean connected() {
    	
        return dbConn != null;
    }
    
    public void closeConnection() {
        if (connected()) {
            try {
                getConnection().close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    
    public void rollbackChanges(){
    	if(connected()){
    		try {
				getConnection().rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
    	}
    }

    public boolean openConnection() {
        String jdbcClassName="com.ibm.db2.jcc.DB2Driver";
        
        dbConn = null;
        try {
            //Load class into memory
            Class.forName(jdbcClassName);
            //Establish connection
            dbConn = DriverManager.getConnection(url, user, password);
            dbConn.setAutoCommit(false);
            
            return true;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return false;
        
    }
    
    public PreparedStatement prepareStatement(String sql) throws SQLException {
        assert connected();
        return dbConn.prepareStatement(sql);
    }

    
    private static Properties ReadConfig() throws FileNotFoundException {
		Properties prop = new Properties();
		String propFileName = "config.properties";
 		InputStream inputStream = DB2Connection.class.getClassLoader().getResourceAsStream(propFileName);
 
		if (inputStream != null) {
			try {
				prop.load(inputStream);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
		}
		return prop;
	}
}
