package com.dsv.road.customerorder.jdbc;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsv.road.customerorder.exception.CustomerOrderException;
import com.dsv.road.customerorder.model.OrderLite;
import com.dsv.road.customerorder.model.OrderRow;
import com.dsv.road.customerorder.xml.v1.Order;

public class CustomerOrderPersistenceReadTest {

	DB2Connection db2 = new DB2Connection();
	CustomerOrderInfoPersistence coiDb = null;
	CustomerOrderPersistence coDb = null;
	ArrayList<OrderLite> list = null;
	Order order = null;
	final Logger logger = LoggerFactory
			.getLogger(CustomerOrderPersistenceReadTest.class);

	public CustomerOrderPersistenceReadTest() {

	}

	@Before
	public void given() {
		logger.info("Setup for the test");
		db2.openConnection();
		coiDb = new CustomerOrderInfoPersistence(db2.getConnection());
		coDb = new CustomerOrderPersistence(db2.getConnection());
	}

	@After
	public void cleanup() {
		db2.rollbackChanges();
		db2.closeConnection();
	}

	@Test
	public void testReadDetails() {
		logger.info("Running the test: ReadDetails");
		logger.info("Checking the connection to the database");
		logger.info("Doing the WHEN statement");
		CustomerOrderInstance ci = new CustomerOrderInstance();
		Order order = ci.getOrder();
		OrderRow orderRow = null;
		try {
			orderRow = coDb.insertOrder(order);
			Assert.assertNotNull(orderRow);
			order = orderRow.getOrder();
		} catch (CustomerOrderException e1) {
			Assert.fail("Unit test for reading CustomerOrder details cannot create new costumerOrder");

			e1.printStackTrace();
		}

		Order loadedOrder = null;
		OrderRow loadedOrderRow = null;
		try {
			loadedOrderRow = coDb.getCustomerOrder(Long.valueOf(order
					.getOrderId()));
			Assert.assertNotNull(loadedOrderRow);
			loadedOrder = loadedOrderRow.getOrder();
			Assert.assertNotNull(loadedOrder);
		} catch (CustomerOrderException e) {
			logger.error("Test failed. CustomerOrderException thrown: {}",
					e.getMessage());
			Assert.fail(e.getMessage());
		}

		logger.info("Checking the success criteria");

		logger.info("Checking that the returned order is not null: ");
		assertNotNull(loadedOrder);
		logger.info("Checking that the returned order id is greater than 0...");
		Assert.assertNotNull(loadedOrder.getOrderId());
		logger.info("Checking that the returned order Message is not null...");
		assertNotNull(loadedOrder.getDeliverDate());

	}

}
