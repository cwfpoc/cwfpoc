package com.dsv.road.customerorder.jdbc;

import static org.junit.Assert.assertNotNull;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsv.road.customerorder.exception.CustomerOrderException;
import com.dsv.road.customerorder.jdbc.CustomerOrderPersistence;
import com.dsv.road.customerorder.model.OrderRow;
import com.dsv.road.customerorder.model.OrderLite;

@Ignore
public class CustomerOrderPersistenceListTest {

	DB2Connection db2 = new DB2Connection();
	ArrayList<OrderLite> list = null;
	OrderRow order = null;
	CustomerOrderInfoPersistence coiDb = null;
	CustomerOrderPersistence coDb = null;
    
	final Logger logger = LoggerFactory.getLogger(CustomerOrderPersistenceListTest.class);
	
	
	public CustomerOrderPersistenceListTest() {
		
	}
	
	@Before
	public void before() {
		logger.info("Setup for the test");
		db2.openConnection();
		coiDb = new CustomerOrderInfoPersistence(db2.getConnection());
		coDb = new CustomerOrderPersistence(db2.getConnection());
	}
	
	@After
	public void after() {
		db2.rollbackChanges();
		db2.closeConnection();
	}
	
	@Test
	public void testTableExists() {
        PreparedStatement ps;
		try {
			ps = db2.prepareStatement("SELECT COUNT(*) FROM ODB.CUSTOMER_ORDER");
	        ResultSet rs = ps.executeQuery();
	        if (rs.next()) {
	        	logger.info("Found {} existing rows in the database", rs.getInt(1));
	        } else {
	        	logger.error("Table did not exists");
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
	}
	@Test
	public void testListCustomerOrders() {
		logger.info("Running Test to update object");
		
		logger.info("Checking the connection to the database");
		assertNotNull(db2.getConnection());
		CustomerOrderInstance coInstance = new CustomerOrderInstance();
		try {
			coDb.insertOrder(coInstance.getOrder());
		} catch (CustomerOrderException e) {
			Assert.fail("Could not insert Customer Order");
			e.printStackTrace();
		}
		
		doWhenActionLoadList();
		doThenActionCheckList();
	}
	
	public void doWhenActionLoadList() {
		logger.info("Doing the WHEN statement");
		
		try {
			list = coiDb.listCustomerOrders();
		} catch (CustomerOrderException e) {
			logger.error("Test failed. CustomerOrderException thrown: {}", e.getMessage());
			Assert.fail(e.getMessage());
		} 
	}
	
	public void doThenActionCheckList() {
		logger.info("Checking the success criteria");
		
		logger.info("Checking that the returned list is more than 0 items (actual {})", list.size());
		Assert.assertTrue(list.size() >= 0);
	}
	
	
	
	
 }
