package com.dsv.road.customerorder.jdbc;

import java.util.ArrayList;

import org.junit.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsv.road.customerorder.exception.CustomerOrderException;
import com.dsv.road.customerorder.model.OrderLite;
import com.dsv.road.customerorder.model.OrderRow;
import com.dsv.road.customerorder.xml.v1.Order;

@Ignore
public class CustomerOrderPersistenceUpdateTest {

	DB2Connection db2 = new DB2Connection();
	CustomerOrderInfoPersistence coiDb = null;
	CustomerOrderPersistence coDb = null;
	ArrayList<OrderLite> list = null;
	Order order = null;
	String testName = "OName";
	final Logger logger = LoggerFactory
			.getLogger(CustomerOrderPersistenceUpdateTest.class);
	private String NEW_TEST_ID = "TESTTESTTEST";

	public CustomerOrderPersistenceUpdateTest() {

	}

	@Before
	public void given() {
		logger.info("Setup for the test");
		db2.openConnection();
		coiDb = new CustomerOrderInfoPersistence(db2.getConnection());
		coDb = new CustomerOrderPersistence(db2.getConnection());
	}

	@After
	public void cleanup() {
		db2.rollbackChanges();
		db2.closeConnection();
	}

	@Test
	public void testUpdate() {
		logger.info("Running the test");

		logger.info("Loading the list of customers");
		CustomerOrderInstance ci = new CustomerOrderInstance();
		Order order = ci.getOrderWithShipperName(testName);
		OrderRow orderRow = null;
		try {

			orderRow = coDb.insertOrder(order);
			Assert.assertNotNull(orderRow);
			order = orderRow.getOrder();
		} catch (CustomerOrderException e1) {
			Assert.fail("Unit test for reading CustomerOrderInfo details cannot create new costumerOrder");

			e1.printStackTrace();
		}

		logger.info("Selected customer order {} for test", order.getOrderId());

		try {
			OrderRow oldOrderRow = coDb.getCustomerOrder(Long.valueOf(order
					.getOrderId()));
			Order oldOrder = oldOrderRow.getOrder();
			order.getShipperParty().getName().setName1("Nname");

			OrderRow newOrderRow = coDb.updateOrder(orderRow.getId(), order);

			Order newOrder = newOrderRow.getOrder();
			logger.info("old ({}) and new ({})", oldOrder.getOrderId(), newOrder.getOrderId());
			Assert.assertEquals(oldOrder.getOrderId(), newOrder.getOrderId());
			logger.info("old ({}) and new ({})", oldOrder.getShipperParty().getName().getName1(), newOrder.getShipperParty().getName().getName1());
			Assert.assertFalse(oldOrder.getShipperParty().getName().getName1()
					.equals(newOrder.getShipperParty().getName().getName1()));

		} catch (CustomerOrderException e) {
			logger.info("CustomerOrder exception: {}", e.getMessage());
			Assert.fail("unable to update order");
			e.printStackTrace();
		}

	}

	public void loadCustomerOrders() {

		try {
			list = coiDb.listCustomerOrders();
		} catch (CustomerOrderException e) {
			logger.error("Test failed. CustomerOrderException thrown: {}",
					e.getMessage());
			Assert.fail(e.getMessage());
		}
	}

}
