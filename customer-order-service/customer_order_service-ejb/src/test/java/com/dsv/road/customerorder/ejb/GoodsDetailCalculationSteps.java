package com.dsv.road.customerorder.ejb;

import static org.junit.Assert.assertEquals;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.dsv.road.shared.customerorder.dto.DtoGoodsDetails;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class GoodsDetailCalculationSteps {

	List<DtoGoodsDetails> goodsDetails = new ArrayList<DtoGoodsDetails>();
	@Given("^the following orderlines$")
	public void the_following_orderlines(List<DtoGoodsDetails> list) throws Throwable {
		for (DtoGoodsDetails detail : list) {
			DtoGoodsDetails newDetail = new DtoGoodsDetails();
			newDetail.setNumberOfPackages(detail.getNumberOfPackages());
			newDetail.setPackageType(detail.getPackageType());
			newDetail.setGrossWeight(detail.getGrossWeight());
			newDetail.setGrossVolume(detail.getGrossVolume());
			newDetail.setLoadingMeters(detail.getLoadingMeters());
		    
			goodsDetails.add(detail);
		}
	}

	@Then("^the summarized orderline should be$")
	public void the_summarized_orderline_should_be(List<DtoGoodsDetails> expectedList) throws Throwable {
		// we only expect one object
		assertEquals(1,expectedList.size());
		DtoGoodsDetails expectedSum = expectedList.get(0);
		DtoGoodsDetails sum = GoodsDetailCalculation.SumGoodsDetail(goodsDetails);
		assertEquals(expectedSum.getNumberOfPackages(), sum.getNumberOfPackages());
		assertEquals(expectedSum.getPackageType(), sum.getPackageType());
		assertEquals(expectedSum.getGrossWeight(), sum.getGrossWeight());
		assertEquals(expectedSum.getGrossVolume(), sum.getGrossVolume());
		assertEquals(expectedSum.getLoadingMeters(), sum.getLoadingMeters());
	}
}
