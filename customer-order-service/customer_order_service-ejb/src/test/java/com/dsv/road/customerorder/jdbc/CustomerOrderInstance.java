package com.dsv.road.customerorder.jdbc;

import com.dsv.road.customerorder.xml.v1.Address;
import com.dsv.road.customerorder.xml.v1.DateType;
import com.dsv.road.customerorder.xml.v1.Name;
import com.dsv.road.customerorder.xml.v1.Order;
import com.dsv.road.customerorder.xml.v1.Party;
import com.dsv.shared.joda.JodaUtility;

public class CustomerOrderInstance {
	public CustomerOrderInstance() {
	}

	String testID = "42";
	
	public Order getOrder(){
			
			return getOrderWithShipperName("ShipperName");
	}
	
	public Order getOrderWithShipperName(String shipperNameString){
		
		Order order = new Order();
		order.setOrderId(testID);
		
		DateType pickupDateType = new DateType();
		pickupDateType.setDate(JodaUtility.getLocalDateFromString("2015-04-01"));
		order.setPickupDate(pickupDateType);
		

		DateType deliverDateType = new DateType();
		deliverDateType.setDate(JodaUtility.getLocalDateFromString("2015-05-03"));
		order.setDeliverDate(deliverDateType);
		
		Party shipperParty = new Party();
		Name shipperName = new Name();
		shipperName.setName1(shipperNameString);
		shipperParty.setName(shipperName);
		Address shipperAddress = new Address();
		shipperAddress.setAddress1("shipperStreet 4");
		shipperParty.setAddress(shipperAddress);
		order.setShipperParty(shipperParty);
		
		Party consigneeParty = new Party();
		Name consigneeName = new Name();
		consigneeName.setName1("ShipperName");
		consigneeParty.setName(consigneeName);
		Address consigneeAddress = new Address();
		consigneeAddress.setAddress1("deliverStreet 1");
		shipperParty.setAddress(consigneeAddress);
		order.setConsigneeParty(consigneeParty);
		return order;
	}
	
}
