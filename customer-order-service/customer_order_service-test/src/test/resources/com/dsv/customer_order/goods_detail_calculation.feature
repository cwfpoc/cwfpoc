@CWFCOM-87
Feature: Summarize order line
	#As a CSA I need to register goods information on order line level on a customer order, so that I can specify the goods to be transported.

	
	@CWFCOM-179 @CWFCOM-152
	Scenario: CWFCOM-87: Sum orderlines that has different package types
		Given the following orderlines
		  | numberOfPackages | packageType | grossWeight | grossVolume | loadingMeters |  
		  | 1                | T1          | 1.123       | 1.123       | 1.123         |  
		  | 2                | T2          | 3.321       | 3.321       | 3.321         |  

		Then the summarized orderline should be
		  | numberOfPackages | packageType | grossWeight | grossVolume | loadingMeters |  
		  | 3                | CLL         | 4.444       | 4.444       | 4.444         |  
	
	@CWFCOM-178 @CWFCOM-152
	Scenario: CWFCOM-87: Sum orderlines that has same package types
		Given the following orderlines
		  | numberOfPackages | packageType | grossWeight | grossVolume | loadingMeters |  
		  | 1                | T1          | 1.123       | 1.123       | 1.123         |  
		  | 2                | T1          | 3.321       | 3.321       | 3.321         |  

		Then the summarized orderline should be
		  | numberOfPackages | packageType | grossWeight | grossVolume | loadingMeters |  
		  | 3                | T1          | 4.444       | 4.444       | 4.444         |  

	
