package com.dsv.customer_order;

import static org.junit.Assert.assertEquals;

import javax.ws.rs.core.MediaType;
import org.apache.wink.client.ClientResponse;
import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsv.road.shared.customerorder.dto.DtoOrder;
import com.dsv.shared.joda.JodaUtility;
import com.google.gson.Gson;


public class OrdersResourceTest {
	final Logger logger = LoggerFactory.getLogger(OrdersResourceTest.class);
	private static String URL = ClientApplication.baseURL() + "/orders";
	private static RestClient restClient = ClientApplication.getJAXBClient();
	Gson gson = JodaUtility.getGson();
	

	@Test
	public void get_service_option() {
		Resource resource = restClient.resource(URL + "/1");
		ClientResponse clientResponse = resource.options();
		assertEquals(200, clientResponse.getStatusCode());
	}

	//@Test
	public void create_using_json() throws Exception {
		Resource resource = restClient.resource(URL);
		// create valid element
		String jsonStr = gson.toJson(CustomerOrderInstance.getDtoOrder());
		System.out.println("Creating object: " + jsonStr);
		ClientResponse clientResponse = resource
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.post(jsonStr);
		String responseStr = clientResponse.getHeaders().getFirst("Location");
		System.out.println("Reponse: " + responseStr);
		Assert.assertNotEquals(
				clientResponse.getHeaders().getFirst("Location"),
				"Missing Location header");
		assertEquals(201, clientResponse.getStatusCode());
		// create invalid element.
		jsonStr = gson.toJson(CustomerOrderInstance.getOrder());
		clientResponse = resource.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.post(jsonStr);
		System.out.println("create wrong element = "
				+ clientResponse.getStatusCode());
		Assert.assertEquals(400, clientResponse.getStatusCode());
	}

	@Test
	public void get_using_json() throws Exception {
		Resource resource = restClient.resource(URL);
		
		String jsonStr = gson.toJson(CustomerOrderInstance.getDtoOrder());
		ClientResponse clientResponseCreate = resource
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.post(jsonStr);
		String responseURI = clientResponseCreate.getHeaders()
				.getFirst("Location");
		System.out.println("Response of create: " + responseURI);
		
		// Read newly created.
		resource = restClient.resource(responseURI);
		
		ClientResponse clientResponse = resource
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).get();
		System.out.println("STATUS CODE for json get: "
				+ clientResponse.getStatusCode());
		DtoOrder dtoOrder = gson.fromJson(clientResponse.getEntity(String.class), DtoOrder.class);
		Assert.assertNotNull(dtoOrder);
		Assert.assertEquals(200, clientResponse.getStatusCode());
		// read something that is not there.
		resource = restClient.resource(URL + "/NotExisting");
		clientResponse = resource.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).get();
		System.out
				.println("STATUS CODE for json get for not existing element: "
						+ clientResponse.getStatusCode());
		Assert.assertEquals(404, clientResponse.getStatusCode());
	}

	@Test
	public void update_using_json() throws Exception {
		String newDeliveryDate = "2020-12-12";
		Resource resource = restClient.resource(URL);
		String jsonStr = gson.toJson(CustomerOrderInstance.getDtoOrder());

		ClientResponse clientResponse = resource
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.post(jsonStr);
		resource = restClient.resource(clientResponse.getHeaders()
				.getFirst("Location"));
		//Get order to modify
		clientResponse = resource
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).get();
		DtoOrder dtoOrder = gson.fromJson(clientResponse.getEntity(String.class), DtoOrder.class);
		dtoOrder.getDeliverDate().setDate(newDeliveryDate);
		jsonStr = gson.toJson(dtoOrder);
		//Modify order
		clientResponse = resource
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.put(jsonStr);
		assertEquals(200, clientResponse.getStatusCode());
		//check if modified
		clientResponse = resource
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).get();
		dtoOrder = null;
		dtoOrder = gson.fromJson(clientResponse.getEntity(String.class), DtoOrder.class);
		
		Assert.assertEquals(dtoOrder.getDeliverDate().getDate(),newDeliveryDate);;
	}

	@Test
	public void delete_using_json() throws Exception {
		Resource resource = restClient.resource(URL);
		String jsonStr = gson.toJson(CustomerOrderInstance.getDtoOrder());
		ClientResponse clientResponseCreate = resource
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.post(jsonStr);
		// delete created element
		String url = (clientResponseCreate.getHeaders().getFirst("Location"));
		resource = restClient.resource(url);
		ClientResponse clientResponse = resource
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).delete();
		assertEquals(200, clientResponse.getStatusCode());
		// Check if deleted
		clientResponse = resource.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).get();
		System.out.println("deleted read status code: "
				+ clientResponse.getStatusCode());
		Assert.assertEquals(404, clientResponse.getStatusCode());
	}
}
