package com.dsv.customer_order;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import org.apache.wink.client.ClientConfig;
import org.apache.wink.client.RestClient;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

public class ClientApplication extends Application {
    private Set<Object> singletons = Collections.emptySet();

	@Override
    public Set<Object> getSingletons() {
        return singletons;
    }

    public void setSingletons(final Set<Object> singletons) {
        this.singletons = singletons;
    }
    
    public static RestClient getJAXBClient() {
		ClientApplication clientApplication = new ClientApplication();
		Set<Object> s = new HashSet<Object>();
		s.add(new JacksonJaxbJsonProvider());
		clientApplication.setSingletons(s);
		ClientConfig clientConfig = new ClientConfig()
				.applications(clientApplication);
		return new RestClient(clientConfig);
    }
    
    public static String baseURL() {
		String contextPath = System.getProperty("contextPath",
				"/customer_order_service-web");
		String httpPort = System.getProperty("httpPort", "9090");
		return "http://localhost" + ":" + httpPort + contextPath + "/rest";
    }
}
