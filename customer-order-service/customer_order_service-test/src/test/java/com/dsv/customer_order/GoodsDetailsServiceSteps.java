package com.dsv.customer_order;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.apache.wink.client.ClientResponse;
import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dsv.road.shared.customerorder.dto.DtoGoodsDetails;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class GoodsDetailsServiceSteps {
	final Logger logger = LoggerFactory.getLogger(GoodsDetailsServiceSteps.class);
	private static String URL = ClientApplication.baseURL() + "/goods_details";
	private static RestClient restClient = ClientApplication.getJAXBClient();

	List<DtoGoodsDetails> goodsDetails = new ArrayList<DtoGoodsDetails>();
	@Given("^the following orderlines$")
	public void the_following_orderlines(List<DtoGoodsDetails> list) throws Throwable {
		for (DtoGoodsDetails detail : list) {
			DtoGoodsDetails newDetail = new DtoGoodsDetails();
			newDetail.setNumberOfPackages(detail.getNumberOfPackages());
			newDetail.setPackageType(detail.getPackageType());
			newDetail.setGrossWeight(detail.getGrossWeight());
			newDetail.setGrossVolume(detail.getGrossVolume());
			newDetail.setLoadingMeters(detail.getLoadingMeters());
		    
			goodsDetails.add(detail);
		}
	}

	@Then("^the summarized orderline should be$")
	public void the_summarized_orderline_should_be(List<DtoGoodsDetails> expectedList) throws Throwable {
		// we only expect one object
		assertEquals(1,expectedList.size());
		DtoGoodsDetails expectedSum = expectedList.get(0);

		// call the service
		Resource resource = restClient.resource(URL + "/sum");
		ClientResponse clientResponse = resource.
				contentType(MediaType.APPLICATION_JSON).
				accept(MediaType.APPLICATION_JSON).
				post(goodsDetails);
		
		// Assert success and deserialize response 
		assertEquals(200, clientResponse.getStatusCode());
		DtoGoodsDetails sum = clientResponse.getEntity(DtoGoodsDetails.class);

		// assert that it match expected
		assertEquals(expectedSum.getNumberOfPackages(), sum.getNumberOfPackages());
		assertEquals(expectedSum.getPackageType(), sum.getPackageType());
		assertEquals(expectedSum.getGrossWeight(), sum.getGrossWeight());
		assertEquals(expectedSum.getGrossVolume(), sum.getGrossVolume());
		assertEquals(expectedSum.getLoadingMeters(), sum.getLoadingMeters());
	}
	
}
