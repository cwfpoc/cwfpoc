package com.dsv.customer_order;

import java.math.BigDecimal;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import com.dsv.road.customerorder.xml.v1.Address;
import com.dsv.road.customerorder.xml.v1.CashOnDelivery;
import com.dsv.road.customerorder.xml.v1.DateType;
import com.dsv.road.customerorder.xml.v1.Goods;
import com.dsv.road.customerorder.xml.v1.GoodsDetails;
import com.dsv.road.customerorder.xml.v1.Instruction;
import com.dsv.road.customerorder.xml.v1.Name;
import com.dsv.road.customerorder.xml.v1.Order;
import com.dsv.road.customerorder.xml.v1.Party;
import com.dsv.road.customerorder.xml.v1.Reference;
import com.dsv.road.customerorder.xml.v1.TermsOfDelivery;
import com.dsv.road.shared.customerorder.dto.DtoAddress;
import com.dsv.road.shared.customerorder.dto.DtoDateType;
import com.dsv.road.shared.customerorder.dto.DtoName;
import com.dsv.road.shared.customerorder.dto.DtoOrder;
import com.dsv.road.shared.customerorder.dto.DtoParty;

public class CustomerOrderInstance {

	private static String testID = "42";
	
	public static DtoOrder getDtoOrder(){
			
		DtoOrder order = getDtoOrderWithShipperName("ShipperName");
		return order;
	}
	
	public static DtoOrder getDtoOrderWithShipperName(String shipperNameString){
		
		DtoOrder order = new DtoOrder();
		order.setOrderId(testID);
		DtoDateType pickupDateType = new DtoDateType("2015-04-01");
		pickupDateType.setTimeFrom("00:00");
		pickupDateType.setTimeTo("00:00");
		order.setPickupDate(pickupDateType);
		
		DtoDateType deliverDateType = new DtoDateType("2015-05-03");
		deliverDateType.setTimeFrom("00:00");
		deliverDateType.setTimeTo("00:00");
		order.setDeliverDate(deliverDateType);
		
		DtoParty shipperParty = new DtoParty();
		DtoName shipperName = new DtoName();
		shipperName.setName1(shipperNameString);
		shipperParty.setName(shipperName);
		DtoAddress shipperAddress = new DtoAddress();
		shipperAddress.setAddress1("shipperStreet 4");
		shipperParty.setAddress(shipperAddress);
		order.setShipperParty(shipperParty);
		
		DtoParty consigneeParty = new DtoParty();
		DtoName consigneeName = new DtoName();
		consigneeName.setName1("ShipperName");
		consigneeParty.setName(consigneeName);
		DtoAddress consigneeAddress = new DtoAddress();
		consigneeAddress.setAddress1("deliverStreet 1");
		shipperParty.setAddress(consigneeAddress);
		order.setConsigneeParty(consigneeParty);
		return order;
	}
	
	public static Order getOrder(){
		int i = 42;
		Order o = new Order(String.valueOf(i));
		
		o.addReference(new Reference("External id", "external id " + i));
		o.addReference(new Reference("Dinas id", "Dinas id " + i));
		
		o.setConsigneeParty(new Party(1, new Name("ConsigneeParty on order " + i)));
		o.getConsigneeParty().setAddress(new Address("DK", "Consignee address"));
		o.setDeliverParty(new Party(2, new Name("DeliverParty on order " + i)));
		o.getDeliverParty().setAddress(new Address("DK", "Delivery address"));
		o.setPickupParty(new Party(3, new Name("PickupParty on order " + i)));
		o.getPickupParty().setAddress(new Address("DK", "Pickup address"));
		o.setShipperParty(new Party(4, new Name("ShipperParty on order " + i)));
		o.getShipperParty().setAddress(new Address("DK", "Shipper address"));
		
		
		GoodsDetails gd = new GoodsDetails(1000, "Pallets", "Test delivery");
		gd.setGrossVolume(new BigDecimal(100));
		gd.setGrossWeight(new BigDecimal(50));
		gd.setHeight(new BigDecimal(1000));
		gd.setWidth(new BigDecimal(200));
		gd.setLoadingMeters(new BigDecimal(1));
		Goods g = new Goods(gd);

		gd = new GoodsDetails(500, "Pallets", "Vacum cleaners");
		gd.setGrossVolume(new BigDecimal(50));
		gd.setGrossWeight(new BigDecimal(25));
		gd.setHeight(new BigDecimal(500));
		gd.setWidth(new BigDecimal(200));
		gd.setLoadingMeters(new BigDecimal(0.5));
		g.addGoodsDetails(gd);
		gd = new GoodsDetails(500, "Pallets", "Pipes");
		gd.setGrossVolume(new BigDecimal(50));
		gd.setGrossWeight(new BigDecimal(25));
		gd.setHeight(new BigDecimal(500));
		gd.setWidth(new BigDecimal(200));
		gd.setLoadingMeters(new BigDecimal(0.5));
		g.addGoodsDetails(gd);
		o.addGoods(g);
		gd = new GoodsDetails(1000, "Pallets", "Summary");
		gd.setGrossVolume(new BigDecimal(100));
		gd.setGrossWeight(new BigDecimal(50));
		gd.setHeight(new BigDecimal(1000));
		gd.setWidth(new BigDecimal(200));
		gd.setLoadingMeters(new BigDecimal(1));
		o.setGoodsSummary(gd);
		
		o.setCashOnDelivery(new CashOnDelivery("DKK", new BigDecimal(1000)));
		o.setTermsOfDelivery(new TermsOfDelivery("Shipper", "Reponsible until delivery"));
		
		o.setPickupDate(new DateType(new LocalDate(2015,4,1), new LocalTime(8,0), new LocalTime(16,0)));
		o.setDeliverDate(new DateType(new LocalDate(2015,5,1), new LocalTime(8,0), new LocalTime(16,0)));
		
		o.addDeliverInstruction(new Instruction("Call 12345678 30 minutes before"));
		o.addDeliverInstruction(new Instruction("Call shipper after to confirm cash on delivery payment"));
		
		o.addPickupInstruction(new Instruction("Peter knows where the goods are. Call him at 12345678"));
		o.addPickupInstruction(new Instruction("Incase Peter is ill, call Hans at 87654321"));
		return o;
	}
	
}
