/**
 * 
 */
package com.dsv.road.operationaladdress.xml2;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

/**
 * @author ext.jesper.munkholm
 *
 */
@XmlRootElement(name = "CreditAmount", namespace = "com.dsv.road.operationaladdress.xml2")
@XmlAccessorType(XmlAccessType.FIELD)
public class CreditAmount {

     @XmlAttribute(name = "CurrencyCode", required = true)
     protected String currencyCode;
	 @XmlValue
     protected BigDecimal value;
	 
	/**
	 * 
	 */
	public CreditAmount() {
	}

	/**
	 * @param currencyCode
	 * @param value
	 */
	public CreditAmount(String currencyCode, BigDecimal value) {
		this.currencyCode = currencyCode;
		this.value = value;
	}

	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * @param currencyCode the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @return the value
	 */
	public BigDecimal getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	 
	 
}
