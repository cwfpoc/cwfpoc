/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsv.road.operationaladdress.view;

import com.dsv.road.customerorder.model.Goods;
import com.dsv.road.customerorder.model.GoodsDetails;
import com.dsv.road.customerorder.model.Order;
import com.dsv.road.operationaladdress.service.OrderService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.CellEditEvent;

/**
 *
 * @author jemun
 */
@ManagedBean(name="orderView")
@SessionScoped
public class OrderView {

    private String orderId;
    private Order order;

    @ManagedProperty("#{orderService}")
    private OrderService orderService;

    @PostConstruct
    public void init() {
        order = getOrderService().getDummyOrder();

    }

    /**
     * @return the orderId
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * @param orderId the orderId to set
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     * @return the order
     */
    public Order getOrder() {
        return order;
    }

    /**
     * @param order the order to set
     */
    public void setOrder(Order order) {
        this.order = order;
    }

    /**
     * @return the orderService
     */
    public OrderService getOrderService() {
        return orderService;
    }

    /**
     * @param orderService the orderService to set
     */
    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public void addOrderLine() {
        GoodsDetails gd = new GoodsDetails(1, "EUR", "");
        gd.setGrossVolume(new BigDecimal(0));
        gd.setGrossWeight(new BigDecimal(0));
        gd.setHeight(new BigDecimal(0));
        gd.setWidth(new BigDecimal(0));
        gd.setLoadingMeters(new BigDecimal(0));
        
        Goods g = new Goods(gd);
        g.setGoodsContainedList(new ArrayList<GoodsDetails>());
        
        order.getGoodsList().add(g);
    }

    public void deleteOrderLine(Goods goodsItem) {
        order.getGoodsList().remove(goodsItem);
    }

    public void addDimension(Goods goodsItem) {
        System.out.println("Adding dimension");
        if (goodsItem.getGoodsContainedList().isEmpty()) {
            GoodsDetails gd = new GoodsDetails(goodsItem.getGoodsDetails().getNumberOfPackages(), 
                                                goodsItem.getGoodsDetails().getPackageType(), 
                                                goodsItem.getGoodsDetails().getDescription());
            gd.setGrossVolume(new BigDecimal(0));
            gd.setGrossWeight(new BigDecimal(0));
            gd.setLoadingMeters(new BigDecimal(0));
            gd.setHeight(new BigDecimal(0));
            gd.setWidth(new BigDecimal(0));
            gd.setLength(new BigDecimal(0));
            goodsItem.getGoodsContainedList().add(gd);
        } else {
            GoodsDetails gd = new GoodsDetails(1, 
                                                "", 
                                                "");
            gd.setGrossVolume(new BigDecimal(0));
            gd.setGrossWeight(new BigDecimal(0));
            gd.setLoadingMeters(new BigDecimal(0));
            gd.setHeight(new BigDecimal(0));
            gd.setWidth(new BigDecimal(0));
            gd.setLength(new BigDecimal(0));
            goodsItem.getGoodsContainedList().add(gd);
        }
    }
    
    public void deleteDimension(Goods goodsItem, GoodsDetails dimension) {
        System.out.println("Deleting dimension");
        goodsItem.getGoodsContainedList().remove(dimension);
    }
    
    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
    public List<String> getPackageTypes() {
        return orderService.getPackageTypes();
    }
    
    public void onCellEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();
         
        if(newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cell Changed", "Old: " + oldValue + ", New:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
    
    public void updateSummary(Object o) {
        System.out.println("Updating summary");
        
    }
}
