/**
 * 
 */
package com.dsv.road.operationaladdress.xml2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author ext.jesper.munkholm
 *
 */
@XmlRootElement(name = "AccessGeneral", namespace = "com.dsv.road.operationaladdress.xml2")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccessGeneral {

	@XmlElement(name="Access")
	protected AccessType access;
	
	/**
	 * 
	 */
	public AccessGeneral() {
		
	}

	
	/**
	 * @param access
	 */
	public AccessGeneral(AccessType access) {
		this.access = access;
	}


	/**
	 * @return the access
	 */
	public AccessType getAccess() {
		return access;
	}

	/**
	 * @param access the access to set
	 */
	public void setAccess(AccessType access) {
		this.access = access;
	}
	
	
}
