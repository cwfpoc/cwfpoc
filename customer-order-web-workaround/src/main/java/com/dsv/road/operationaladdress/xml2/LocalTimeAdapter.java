/**
 * 
 */
package com.dsv.road.operationaladdress.xml2;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * @author ext.jesper.munkholm
 *
 */
public class LocalTimeAdapter extends XmlAdapter<String, LocalTime> {
	
	static DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm");
	
	public LocalTime unmarshal(String v) throws Exception {
        return LocalTime.parse(v, fmt);
    }
 
    public String marshal(LocalTime v) throws Exception {
        return v.toString(fmt);
    }
}
