/**
 * 
 */
package com.dsv.road.operationaladdress.xml2;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author ext.jesper.munkholm
 *
 */
@XmlRootElement(name = "AccessDetailsOpen", namespace = "com.dsv.road.operationaladdress.xml2")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccessDetailsOpen {

	 @XmlElement(name = "OpenPeriod", required = true)
	 @XmlElementWrapper(name="OpenPeriodList" )
     protected List<AccessDetailsOpenPeriod> openPeriods;
     @XmlAttribute(name = "Weekday", required = true)
     protected String weekday;
     
     public AccessDetailsOpen() {
    	 
     };
     
     public AccessDetailsOpen(String weekday) {
    	 this.weekday = weekday;
     }
     
     public void addAccessDetailsOpenPeriod(AccessDetailsOpenPeriod openPeriod) {
    	 if (openPeriods == null) 
    		 openPeriods = new ArrayList<AccessDetailsOpenPeriod>();
    	 
    	 openPeriods.add(openPeriod);
     }

	/**
	 * @return the weekday
	 */
	public String getWeekday() {
		return weekday;
	}

	/**
	 * @param weekday the weekday to set
	 */
	public void setWeekday(String weekday) {
		this.weekday = weekday;
	}

	/**
	 * @return the openPeriods
	 */
	public List<AccessDetailsOpenPeriod> getOpenPeriods() {
		return openPeriods;
	}
     

}
