/**
 * 
 */
package com.dsv.road.operationaladdress.xml2;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.joda.time.LocalDate;

/**
 * @author ext.jesper.munkholm
 *
 */
@XmlRootElement(name = "AccessType", namespace = "com.dsv.road.operationaladdress.xml2")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccessType {
	
	@XmlElement(name = "Open")
    @XmlElementWrapper(name="OpenList" )
    protected List<AccessDetailsOpen> open;
    @XmlElement(name = "Closed")
    @XmlElementWrapper(name="ClosedList" )
    protected List<AccessDetailsClosed> closed;
    @XmlElement(name = "Instruction")
    @XmlElementWrapper(name="InstructionList" )
    protected List<AccessDetailsInstruction> instruction;
    @XmlAttribute(name = "ValidFromDate", required = true)
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    protected LocalDate validFromDate;
    
    /**
     * 
     */
    public AccessType() {
    	
    }
    
    public AccessType(LocalDate validFromDate) {
    	this.validFromDate = validFromDate;
    }
    
    /**
     * @param ado
     */
    public void addAccessDetailsOpen(AccessDetailsOpen ado) {
    	if (open == null)
    		open = new ArrayList<AccessDetailsOpen>();
    	
    	open.add(ado);
    }
    
    public void addAccessDetailsClosed(AccessDetailsClosed adc) {
    	if (closed == null)
    		closed = new ArrayList<AccessDetailsClosed>();
    	
    	closed.add(adc);
    }
    
    public void addAccessDetailsInstruction(AccessDetailsInstruction adi) {
    	if (instruction == null)
    		instruction = new ArrayList<AccessDetailsInstruction>();
    	
    	instruction.add(adi);
    }

	/**
	 * @return the validFromDate
	 */
	public LocalDate getValidFromDate() {
		return validFromDate;
	}

	/**
	 * @param validFromDate the validFromDate to set
	 */
	public void setValidFromDate(LocalDate validFromDate) {
		this.validFromDate = validFromDate;
	}

	/**
	 * @return the open
	 */
	public List<AccessDetailsOpen> getOpen() {
		return open;
	}

	/**
	 * @return the closed
	 */
	public List<AccessDetailsClosed> getClosed() {
		return closed;
	}

	/**
	 * @return the instruction
	 */
	public List<AccessDetailsInstruction> getInstruction() {
		return instruction;
	}
    
    
}
