/**
 * 
 */
package com.dsv.road.operationaladdress.xml2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

/**
 * @author ext.jesper.munkholm
 *
 */
@XmlRootElement(name = "VatNumber", namespace = "com.dsv.road.operationaladdress.xml2")
@XmlAccessorType(XmlAccessType.FIELD)
public class VATNumber {

	 @XmlValue
     protected String value;
     @XmlAttribute(name = "CountryCode", required = true)
     protected String countryCode;
     
     public VATNumber() {
    	 
     }
	/**
	 * @param value
	 * @param countryCode
	 */
	public VATNumber(String value, String countryCode) {
		this.value = value;
		this.countryCode = countryCode;
	}
	
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	/**
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}
	/**
	 * @param countryCode the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

}
