/**
 * 
 */
package com.dsv.road.operationaladdress.xml2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author ext.jesper.munkholm
 *
 */
@XmlRootElement(name = "Name", namespace = "com.dsv.road.operationaladdress.xml2")
@XmlAccessorType(XmlAccessType.FIELD)
public class Name {

    @XmlElement(name = "Name1", required = true)
    protected String name1;
	@XmlElement(name = "Name2")
    protected String name2;

	/**
	 * 
	 */
	public Name() {
	}

	/**
	 * @param name1
	 */
	public Name(String name1) {
		this.name1 = name1;
	}
	
	/**
	 * @param name1
	 * @param name2
	 */
	public Name(String name1, String name2) {
		this.name1 = name1;
		this.name2 = name2;
	}

	/**
	 * @return the name1
	 */
	public String getName1() {
		return name1;
	}
	/**
	 * @param name1 the name1 to set
	 */
	public void setName1(String name1) {
		this.name1 = name1;
	}
	/**
	 * @return the name2
	 */
	public String getName2() {
		return name2;
	}
	/**
	 * @param name2 the name2 to set
	 */
	public void setName2(String name2) {
		this.name2 = name2;
	}

}
