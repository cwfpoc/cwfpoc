/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsv.road.operationaladdress.view;

import com.dsv.road.operationaladdress.model.PartyInformationInfo;
import com.dsv.road.operationaladdress.service.PartyListService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

/**
 *
 * @author jemun
 */
@ManagedBean(name="partyListView")
@SessionScoped
public class PartyListView implements Serializable {
   
    
    private String searchText;
    
    ArrayList<PartyInformationInfo> partyList;
    private PartyInformationInfo selectedParty;
    List<PartyInformationInfo> filteredPartyList;
    
    @ManagedProperty("#{partyListService}")
    private PartyListService partyListService;

    @PostConstruct
    public void init() {
        partyList = new ArrayList<PartyInformationInfo>();
                
    }
    
    public List<PartyInformationInfo> getPartyList() {
        return partyList;
    }
    
    public List<PartyInformationInfo> getFilteredPartyList() {
        return filteredPartyList;
    }
    
    public void setPartyListService(PartyListService service) {
        partyListService = service;
    }
    
    public void setPartyList(ArrayList<PartyInformationInfo> partyList) {
        this.partyList = partyList;
    }
    
    public void setFilteredPartyList(List<PartyInformationInfo> filteredPartyList) {
        this.filteredPartyList = filteredPartyList;
    }

    /**
     * @return the selectedParty
     */
    public PartyInformationInfo getSelectedParty() {
        return selectedParty;
    }

    /**
     * @param selectedParty the selectedParty to set
     */
    public void setSelectedParty(PartyInformationInfo selectedParty) {
        this.selectedParty = selectedParty;
    }

    public void onRowSelect(SelectEvent event) {
        FacesMessage msg = new FacesMessage("Party selected ", ((PartyInformationInfo) event.getObject()).getPartyId().toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
 
    public void onRowUnselect(UnselectEvent event) {
        FacesMessage msg = new FacesMessage("Party Unselected", ((PartyInformationInfo) event.getObject()).getPartyId().toString());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }   

    public void searchAction() {
        
        partyList.clear();
        boolean result = partyListService.getPartyList(partyList, searchText);
        FacesMessage msg = new FacesMessage("Search updated: " + this.searchText + " Result: " + result);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    /**
     * @return the searchText
     */
    public String getSearchText() {
        return searchText;
    }

    /**
     * @param searchText the searchText to set
     */
    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }
}
