/**
 * 
 */
package com.dsv.road.operationaladdress.xml2;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.SchemaOutputResolver;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * @author ext.jesper.munkholm
 *
 */
public class Test {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws JAXBException 
	 */
	public static void main(String[] args) throws IOException, JAXBException {

		DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyyMMdd HHmm").withZoneUTC();

		Party p = new Party();
		p.setName(new Name("Local Name", "Second line"));
		p.setNameInternational(new NameInternational("International Name", "Second line"));
		p.setLegalEntity(new LegalEntity());
		p.getLegalEntity().addVatNumber(new VATNumber("01010101", "DK"));
		p.getLegalEntity().addVatNumber(new VATNumber("99999999", "NO"));
		p.setAddress(new Address("Local Address", "Second line"));
		p.setAddressInternational(new AddressInternational("International Address", "Second line"));
		
		AccessType at = new AccessType(new LocalDate(2015,12,24));
		
		AccessDetailsOpen ado = new AccessDetailsOpen("Monday");
		ado.addAccessDetailsOpenPeriod(new AccessDetailsOpenPeriod(new LocalTime(0,0), new LocalTime(6,0)));
		ado.addAccessDetailsOpenPeriod(new AccessDetailsOpenPeriod(new LocalTime(20,0), new LocalTime(23,59)));
		at.addAccessDetailsOpen(ado);

		ado = new AccessDetailsOpen("Tuesday");
		ado.addAccessDetailsOpenPeriod(new AccessDetailsOpenPeriod(new LocalTime(8,0), new LocalTime(16,0)));
		at.addAccessDetailsOpen(ado);
		ado = new AccessDetailsOpen("Wednesday");
		ado.addAccessDetailsOpenPeriod(new AccessDetailsOpenPeriod(new LocalTime(8,0), new LocalTime(16,0)));
		at.addAccessDetailsOpen(ado);
		ado = new AccessDetailsOpen("Thursday");
		ado.addAccessDetailsOpenPeriod(new AccessDetailsOpenPeriod(new LocalTime(8,0), new LocalTime(16,0)));
		at.addAccessDetailsOpen(ado);
		ado = new AccessDetailsOpen("Friday");
		ado.addAccessDetailsOpenPeriod(new AccessDetailsOpenPeriod(new LocalTime(8,0), new LocalTime(16,0)));
		at.addAccessDetailsOpen(ado);
		ado = new AccessDetailsOpen("Saturday");
		ado.addAccessDetailsOpenPeriod(new AccessDetailsOpenPeriod(new LocalTime(8,0), new LocalTime(12,0)));
		at.addAccessDetailsOpen(ado);
		
		AccessDetailsClosed adc = new AccessDetailsClosed(new LocalDate(2016,12,24));
		adc.setOpenAt(new LocalTime(8,0));
		adc.setCloseAt(new LocalTime(12,0));
		at.addAccessDetailsClosed(adc);
		adc = new AccessDetailsClosed(new LocalDate(2017,01,01));
		at.addAccessDetailsClosed(adc);
		
		at.addAccessDetailsInstruction(new AccessDetailsInstruction("CALL BEFORE", "555555555"));
		
		p.setAccessGeneral(at);
		p.setAccessPickup(at);
		p.setAccessDeliver(at);
		
		Contact c = new Contact("Janitor");
		c.setAttention("George");
		c.setEmail("george@janitor.com");
		c.addPhone(new Phone("Work", "11111111"));
		c.addPhone(new Phone("International Office", "+47 3333 3333"));
		p.addContact(c);
		c = new Contact("Janitor 2");
		c.setAttention("George the second");
		c.setEmail("georgethesecond@janitor.com");
		c.addPhone(new Phone("Work", "22222222"));
		c.addPhone(new Phone("International Office", "+47 3333 3333"));
		p.addContact(c);

		p.addInstruction(new Instruction("Before delivery", "Call Janitor"));
		p.addInstruction(new Instruction("Before billing", "Check with the financial department"));
		
		p.addCustomsDetail(new CustomsDetails("DK", new CreditAmount("DKK", new BigDecimal(100000))));
		p.addCustomsDetail(new CustomsDetails("NO", new CreditAmount("NOK", new BigDecimal(100000))));
		File tF = new File("text.xml");
		
		try {
			Party.saveToFile(p, tF);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// generate the XSD file
		final File baseDir = new File("testXsd");
		

		final class MySchemaOutputResolver extends SchemaOutputResolver {
		    public Result createOutput(String namespaceUri, String suggestedFileName ) throws IOException {
		        return new StreamResult(new File(baseDir,suggestedFileName));
		    }
		}

		JAXBContext context = JAXBContext.newInstance(Party.class);
		//MySchemaOutputResolver s = new MySchemaOutputResolver();
		//context.
		context.generateSchema(new MySchemaOutputResolver());

	}

}
