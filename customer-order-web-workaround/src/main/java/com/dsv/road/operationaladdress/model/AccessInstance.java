/**
 * 
 */
package com.dsv.road.operationaladdress.model;


import org.joda.time.DateTime;

/**
 * @author ext.jesper.munkholm
 *
 */
public class AccessInstance {

	private String text = null;
	private DateTime from = null;
	private DateTime to = null;
	private String type = null;
	
	public static final String TYPE_OPEN = "OPEN";
	public static final String TYPE_CLOSED = "CLOSED";
			
	/**
	 * @param type
	 */
	public AccessInstance(String type) {
		this.type = type;
	}
	
	/**
	 * @param from
	 * @param to
	 */
	public AccessInstance(DateTime from, DateTime to) {
		this.from = from;
		this.to = to;
	}

	/**
	 * @param type
	 * @param from
	 * @param to
	 */
	public AccessInstance(String type, DateTime from, DateTime to) {
		this.type = type;
		this.from = from;
		this.to = to;
	}
	
	/**
	 * @return the from
	 */
	public DateTime getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(DateTime from) {
		this.from = from;
	}

	/**
	 * @return the to
	 */
	public DateTime getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(DateTime to) {
		this.to = to;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	
	
	
}
