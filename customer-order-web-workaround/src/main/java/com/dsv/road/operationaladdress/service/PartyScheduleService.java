/**
 *
 */
package com.dsv.road.operationaladdress.service;

import java.util.ArrayList;
import java.util.HashMap;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.joda.time.LocalDate;

import com.dsv.road.operationaladdress.model.AccessInstance;
import com.dsv.road.operationaladdress.xml2.AccessDetailsClosed;
import com.dsv.road.operationaladdress.xml2.AccessDetailsInstruction;
import com.dsv.road.operationaladdress.xml2.AccessDetailsOpen;
import com.dsv.road.operationaladdress.xml2.AccessDetailsOpenPeriod;
import com.dsv.road.operationaladdress.xml2.AccessType;
import com.dsv.road.operationaladdress.xml2.Address;
import com.dsv.road.operationaladdress.xml2.AddressInternational;
import com.dsv.road.operationaladdress.xml2.Contact;
import com.dsv.road.operationaladdress.xml2.CreditAmount;
import com.dsv.road.operationaladdress.xml2.CustomsDetails;
import com.dsv.road.operationaladdress.xml2.Instruction;
import com.dsv.road.operationaladdress.xml2.LegalEntity;
import com.dsv.road.operationaladdress.xml2.Name;
import com.dsv.road.operationaladdress.xml2.NameInternational;
import com.dsv.road.operationaladdress.xml2.Party;
import com.dsv.road.operationaladdress.xml2.Phone;
import com.dsv.road.operationaladdress.xml2.VATNumber;
import java.math.BigDecimal;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import org.joda.time.LocalTime;

/**
 * @author ext.jesper.munkholm
 *
 */
@ManagedBean(name = "partyScheduleService")
@ApplicationScoped
public class PartyScheduleService {

    // DateTimeConstants in Iso format (Monday == 1, Sunday == 7)
    public static final String[] weekDays = {"NONE", "Monday", "Tuesday",
        "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

    public ArrayList<AccessInstance> getAccessInstances(LocalDate from,
            LocalDate to, AccessType access) {


        // create hashmap of closed date
        HashMap<String, AccessDetailsClosed> closed = new HashMap<String, AccessDetailsClosed>();
        HashMap<String, AccessDetailsOpen> open = new HashMap<String, AccessDetailsOpen>();

        // load the open weekdays and the closed dates
        for (AccessDetailsClosed adc : access.getClosed()) {
            closed.put(adc.getClosedDate().toString(), adc);
        }

        for (AccessDetailsOpen ado : access.getOpen()) {
            open.put(ado.getWeekday(), ado);
        }
        ArrayList<AccessInstance> list = new ArrayList<AccessInstance>();

        // loop through dates from to to
        LocalDate current = from;
        while (!current.isAfter(to)) {
            // get the weekday of the current date
            String curWeekDay = weekDays[current.getDayOfWeek()];
            AccessDetailsOpen ado = open.get(curWeekDay);
            AccessDetailsClosed adc = closed.get(current.toString());
            // if closed on this date then do not add
            if (ado != null && adc == null) {
                // open for business
                for (AccessDetailsOpenPeriod adop : ado.getOpenPeriods()) {
                    AccessInstance ai = new AccessInstance(AccessInstance.TYPE_OPEN);
                    ai.setFrom(current.toDateTime(adop.getOpenAt()));
                    ai.setTo(current.toDateTime(adop.getClosedAt()));

                    // instructions?
                    list.add(ai);
                }
            }
            
            if (adc != null) {
                if (adc.getOpenAt() != null) {
                    AccessInstance ai = new AccessInstance(AccessInstance.TYPE_CLOSED);
                    ai.setFrom(current.toDateTime(adc.getOpenAt()));
                    ai.setTo(current.toDateTime(adc.getCloseAt()));

                    // instructions?
                    list.add(ai);
                }
            }
            current = current.plusDays(1);
        }
        // access.getInstruction()

        return list;
    }

    public Party getDummyParty() {
        Party p = new Party();
        p.setName(new Name("Local Name", "Second line"));
        p.setNameInternational(new NameInternational("International Name", "Second line"));
        p.setLegalEntity(new LegalEntity());
        p.getLegalEntity().addVatNumber(new VATNumber("01010101", "DK"));
        p.getLegalEntity().addVatNumber(new VATNumber("99999999", "NO"));
        p.setAddress(new Address("Local Address", "Second line"));
        p.setAddressInternational(new AddressInternational("International Address", "Second line"));

        AccessType at = new AccessType(new LocalDate(2015, 12, 24));

        AccessDetailsOpen ado = new AccessDetailsOpen("Monday");
        ado.addAccessDetailsOpenPeriod(new AccessDetailsOpenPeriod(new LocalTime(0, 0), new LocalTime(6, 0)));
        ado.addAccessDetailsOpenPeriod(new AccessDetailsOpenPeriod(new LocalTime(20, 0), new LocalTime(23, 59)));
        at.addAccessDetailsOpen(ado);

        ado = new AccessDetailsOpen("Tuesday");
        ado.addAccessDetailsOpenPeriod(new AccessDetailsOpenPeriod(new LocalTime(8, 0), new LocalTime(16, 0)));
        at.addAccessDetailsOpen(ado);
        ado = new AccessDetailsOpen("Wednesday");
        ado.addAccessDetailsOpenPeriod(new AccessDetailsOpenPeriod(new LocalTime(8, 0), new LocalTime(16, 0)));
        at.addAccessDetailsOpen(ado);
        ado = new AccessDetailsOpen("Thursday");
        ado.addAccessDetailsOpenPeriod(new AccessDetailsOpenPeriod(new LocalTime(8, 0), new LocalTime(16, 0)));
        at.addAccessDetailsOpen(ado);
        ado = new AccessDetailsOpen("Friday");
        ado.addAccessDetailsOpenPeriod(new AccessDetailsOpenPeriod(new LocalTime(8, 0), new LocalTime(16, 0)));
        at.addAccessDetailsOpen(ado);
        ado = new AccessDetailsOpen("Saturday");
        ado.addAccessDetailsOpenPeriod(new AccessDetailsOpenPeriod(new LocalTime(8, 0), new LocalTime(12, 0)));
        at.addAccessDetailsOpen(ado);

        AccessDetailsClosed adc = new AccessDetailsClosed(new LocalDate(2016, 12, 24));
        adc.setOpenAt(new LocalTime(8, 0));
        adc.setCloseAt(new LocalTime(12, 0));
        at.addAccessDetailsClosed(adc);
        adc = new AccessDetailsClosed(new LocalDate(2017, 01, 01));
        at.addAccessDetailsClosed(adc);

        at.addAccessDetailsInstruction(new AccessDetailsInstruction("CALL BEFORE", "555555555"));

        p.setAccessGeneral(at);
        p.setAccessPickup(at);
        p.setAccessDeliver(at);

        Contact c = new Contact("Janitor");
        c.setAttention("George");
        c.setEmail("george@janitor.com");
        c.addPhone(new Phone("Work", "11111111"));
        c.addPhone(new Phone("International Office", "+47 3333 3333"));
        p.addContact(c);
        c = new Contact("Janitor 2");
        c.setAttention("George the second");
        c.setEmail("georgethesecond@janitor.com");
        c.addPhone(new Phone("Work", "22222222"));
        c.addPhone(new Phone("International Office", "+47 3333 3333"));
        p.addContact(c);

        p.addInstruction(new Instruction("Before delivery", "Call Janitor"));
        p.addInstruction(new Instruction("Before billing", "Check with the financial department"));

        p.addCustomsDetail(new CustomsDetails("DK", new CreditAmount("DKK", new BigDecimal(100000))));
        p.addCustomsDetail(new CustomsDetails("NO", new CreditAmount("NOK", new BigDecimal(100000))));

        return p;
    }
}
