/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsv.road.operationaladdress.view;

import com.dsv.road.operationaladdress.model.AccessInstance;
import com.dsv.road.operationaladdress.service.PartyListService;
import com.dsv.road.operationaladdress.service.PartyScheduleService;
import com.dsv.road.operationaladdress.xml2.Party;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.joda.time.LocalDate;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleModel;

/**
 *
 * @author jemun
 */
@ManagedBean(name="partyScheduleView")
@ViewScoped
public class PartyScheduleView {

    private ScheduleModel deliverEventModel;
    private ScheduleModel pickupEventModel;
    private ScheduleModel generalEventModel;
    
    @ManagedProperty("#{partyScheduleService}")
    private PartyScheduleService partyScheduleService;

    
    @PostConstruct
    public void init() {
        deliverEventModel = new DefaultScheduleModel();
        setPickupEventModel(new DefaultScheduleModel());
        setGeneralEventModel(new DefaultScheduleModel());
        
        Party party = partyScheduleService.getDummyParty();
        LocalDate today = new LocalDate();
        LocalDate plusOneYear = today.plusYears(1);
        for (AccessInstance ai : partyScheduleService.getAccessInstances(today, plusOneYear, party.getAccessDeliver())) {
            System.out.println("Adding: " + ai.getFrom().toString() + " - " + ai.getTo().toString());
            deliverEventModel.addEvent(new DefaultScheduleEvent(ai.getType(), ai.getFrom().toDate(), ai.getTo().toDate()));
        }

        for (AccessInstance ai : partyScheduleService.getAccessInstances(today, plusOneYear, party.getAccessPickup())) {
            System.out.println("Adding: " + ai.getFrom().toString() + " - " + ai.getTo().toString());
            getPickupEventModel().addEvent(new DefaultScheduleEvent(ai.getType(), ai.getFrom().toDate(), ai.getTo().toDate()));
        }

        for (AccessInstance ai : partyScheduleService.getAccessInstances(today, plusOneYear, party.getAccessGeneral())) {
            System.out.println("Adding: " + ai.getFrom().toString() + " - " + ai.getTo().toString());
            getGeneralEventModel().addEvent(new DefaultScheduleEvent(ai.getType(), ai.getFrom().toDate(), ai.getTo().toDate()));
        }
        
    }
    public ScheduleModel getDeliverEventModel() {
        return deliverEventModel;
    }
    
    public PartyScheduleService getPartyScheduleService() {
        return partyScheduleService;
    }

    public void setPartyScheduleService(PartyScheduleService partyScheduleService) {
        this.partyScheduleService = partyScheduleService;
    }

    /**
     * @return the pickupEventModel
     */
    public ScheduleModel getPickupEventModel() {
        return pickupEventModel;
    }

    /**
     * @param pickupEventModel the pickupEventModel to set
     */
    public void setPickupEventModel(ScheduleModel pickupEventModel) {
        this.pickupEventModel = pickupEventModel;
    }

    /**
     * @return the generalEventModel
     */
    public ScheduleModel getGeneralEventModel() {
        return generalEventModel;
    }

    /**
     * @param generalEventModel the generalEventModel to set
     */
    public void setGeneralEventModel(ScheduleModel generalEventModel) {
        this.generalEventModel = generalEventModel;
    }
    
    

}
