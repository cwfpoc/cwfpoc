/**
 * 
 */
package com.dsv.road.operationaladdress.xml2;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author ext.jesper.munkholm
 *
 */
@XmlRootElement(name = "LegalEntity", namespace = "com.dsv.road.operationaladdress.xml2")
@XmlAccessorType(XmlAccessType.FIELD)
public class LegalEntity {

	@XmlElement(name = "VATNumber")
    protected List<VATNumber> vatNumbers;

	/**
	 * 
	 */
	public LegalEntity() {
	}

	/**
	 * @return the vatNumber
	 */
	public List<VATNumber> getVatNumbers() {
		return vatNumbers;
	}

	/**
	 * @param vatNumber the vatNumber to set
	 */
	public void setVatNumbers(List<VATNumber> vatNumbers) {
		this.vatNumbers = vatNumbers;
	}

	public void addVatNumber(VATNumber vatNumber) {
		if (this.vatNumbers == null)
			vatNumbers = new ArrayList<VATNumber>();
		
		vatNumbers.add(vatNumber);
	}
}
