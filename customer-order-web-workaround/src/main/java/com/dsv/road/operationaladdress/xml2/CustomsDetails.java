/**
 * 
 */
package com.dsv.road.operationaladdress.xml2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author ext.jesper.munkholm
 *
 */
@XmlRootElement(name = "CustomsDetails", namespace = "com.dsv.road.operationaladdress.xml2")
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomsDetails {

	 @XmlElement(name = "AccountNo", required = true)
     protected String accountNo;
     @XmlElement(name = "CreditAmount", required = true)
     protected CreditAmount creditAmount;
     @XmlAttribute(name = "CountryCode")
     protected String countryCode;
     
	/**
	 * 
	 */
	public CustomsDetails() {
	}

	/**
	 * @param accountNo
	 * @param creditAmount
	 */
	public CustomsDetails(String accountNo, CreditAmount creditAmount) {
		this.accountNo = accountNo;
		this.creditAmount = creditAmount;
	}

	/**
	 * @return the accountNo
	 */
	public String getAccountNo() {
		return accountNo;
	}

	/**
	 * @param accountNo the accountNo to set
	 */
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	/**
	 * @return the creditAmount
	 */
	public CreditAmount getCreditAmount() {
		return creditAmount;
	}

	/**
	 * @param creditAmount the creditAmount to set
	 */
	public void setCreditAmount(CreditAmount creditAmount) {
		this.creditAmount = creditAmount;
	}

	/**
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * @param countryCode the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
	
     
     
}
