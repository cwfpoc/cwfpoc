/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsv.road.operationaladdress.service;

import com.dsv.road.operationaladdress.model.PartyInformationInfo;
import java.util.ArrayList;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author jemun
 */
@ManagedBean(name="partyListService")
@ApplicationScoped
public class PartyListService {
    
    
    public boolean getPartyList(ArrayList<PartyInformationInfo> list, String criteria) {
        // some day this will call the WS
        //ArrayList<PartyInformationInfo> list = new ArrayList<PartyInformationInfo>();
        
        return createDummyParties(list, criteria, 100);
    }
    
    private boolean createDummyParties(ArrayList<PartyInformationInfo> list, String criteria, int count) {
        if (list == null)
            return false;
        
        long id = 0;
        for (PartyInformationInfo pii : list) {
            if (pii.getPartyId() > id) 
                id = pii.getPartyId();
        }
        id++;
        while (count > 0) {
            PartyInformationInfo pii = new PartyInformationInfo();
            pii.setIntAddress("International Address line 1, line 2, line 3, number " + count);
            pii.setLocAddress("Lokal Adresse linie 1, linie 2, linie 3, nr " + count);
            pii.setName("Firma navn [" +criteria + "] " + count);
            pii.setPartyId(id++);
            list.add(pii);
            count--;
        }
        
        return true;
    }
}
