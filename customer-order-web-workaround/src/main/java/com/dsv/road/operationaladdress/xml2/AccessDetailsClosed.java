/**
 * 
 */
package com.dsv.road.operationaladdress.xml2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

/**
 * @author ext.jesper.munkholm
 *
 */
@XmlRootElement(name = "AccessDetailsClosed", namespace = "com.dsv.road.operationaladdress.xml2")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccessDetailsClosed {
	
	//@XmlElement(name = "ClosedDate", required = true)
	@XmlAttribute(name = "Date", required = true)
	@XmlJavaTypeAdapter(LocalDateAdapter.class)
    protected LocalDate closedDate;
    @XmlElement(name = "ClosedUntilDate")
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    protected LocalDate closedUntilDate;
    @XmlElement(name = "OpenAt")
    @XmlJavaTypeAdapter(LocalTimeAdapter.class)
    protected LocalTime openAt;
    @XmlElement(name = "CloseAt")
    @XmlJavaTypeAdapter(LocalTimeAdapter.class)
    protected LocalTime closeAt;
    
    /**
     * 
     */
    public AccessDetailsClosed() {
    	
    }

	/**
	 * @param closedDate
	 */
	public AccessDetailsClosed(LocalDate closedDate) {
		this.closedDate = closedDate;
	}

	/**
	 * @return the closedDate
	 */
	public LocalDate getClosedDate() {
		return closedDate;
	}

	/**
	 * @param closedDate the closedDate to set
	 */
	public void setClosedDate(LocalDate closedDate) {
		this.closedDate = closedDate;
	}

	/**
	 * @return the closedUntilDate
	 */
	public LocalDate getClosedUntilDate() {
		return closedUntilDate;
	}

	/**
	 * @param closedUntilDate the closedUntilDate to set
	 */
	public void setClosedUntilDate(LocalDate closedUntilDate) {
		this.closedUntilDate = closedUntilDate;
	}

	/**
	 * @return the openAt
	 */
	public LocalTime getOpenAt() {
		return openAt;
	}

	/**
	 * @param openAt the openAt to set
	 */
	public void setOpenAt(LocalTime openAt) {
		this.openAt = openAt;
	}

	/**
	 * @return the closeAt
	 */
	public LocalTime getCloseAt() {
		return closeAt;
	}

	/**
	 * @param closeAt the closeAt to set
	 */
	public void setCloseAt(LocalTime closeAt) {
		this.closeAt = closeAt;
	}

	

}
