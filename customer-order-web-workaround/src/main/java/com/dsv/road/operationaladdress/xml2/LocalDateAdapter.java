/**
 * 
 */
package com.dsv.road.operationaladdress.xml2;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.joda.time.LocalDate;

/**
 * @author ext.jesper.munkholm
 *
 */
public class LocalDateAdapter extends XmlAdapter<String, LocalDate> {
	public LocalDate unmarshal(String v) throws Exception {
        return LocalDate.parse(v);
    }
 
    public String marshal(LocalDate v) throws Exception {
        return v.toString();
    }
}
