/**
 * 
 */
package com.dsv.road.operationaladdress.xml2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author ext.jesper.munkholm
 *
 */
@XmlRootElement(name = "AccessPickup", namespace = "com.dsv.road.operationaladdress.xml2")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccessPickup {


	@XmlElement(name="Access")
	protected AccessType access;
	
	/**
	 * 
	 */
	public AccessPickup() {
		
	}

	
	/**
	 * @param access
	 */
	public AccessPickup(AccessType access) {
		this.access = access;
	}


	/**
	 * @return the access
	 */
	public AccessType getAccess() {
		return access;
	}

	/**
	 * @param access the access to set
	 */
	public void setAccess(AccessType access) {
		this.access = access;
	}
	
	
}
