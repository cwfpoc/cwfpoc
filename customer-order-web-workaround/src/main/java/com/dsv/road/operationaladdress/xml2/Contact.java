/**
 * 
 */
package com.dsv.road.operationaladdress.xml2;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author ext.jesper.munkholm
 *
 */
@XmlRootElement(name = "Contact", namespace = "com.dsv.road.operationaladdress.xml2")
@XmlAccessorType(XmlAccessType.FIELD)
public class Contact {

	@XmlElement(name = "Attention")
    protected String attention;
	@XmlElementWrapper(name="Phones")
    @XmlElement(name = "Phone")
    protected List<Phone> phones;
    @XmlElement(name = "Email")
    protected String email;
    @XmlAttribute(name = "Type", required = true)
    protected String type;
    
    /**
     * 
     */
    public Contact() {
    	
    }

	/**
	 * @param type
	 */
	public Contact(String type) {
		this.type = type;
	}

	/**
	 * @return the attention
	 */
	public String getAttention() {
		return attention;
	}

	/**
	 * @param attention the attention to set
	 */
	public void setAttention(String attention) {
		this.attention = attention;
	}

	/**
	 * @return the phones
	 */
	public List<Phone> getPhones() {
		return phones;
	}

	/**
	 * @param phone the phones to set
	 */
	public void setPhones(List<Phone> phones) {
		this.phones = phones;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
    
	public void addPhone(Phone phone) {
		if (phones == null)
			phones = new ArrayList<Phone>();
		
		phones.add(phone);
	}
    
}
