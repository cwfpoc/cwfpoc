/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsv.road.operationaladdress.model;

/**
 *
 * @author jemun
 */
public class PartyInformationInfo {
    
    private Long partyId = null;
    private String name = null;
    private String locAddress = null;
    private String intAddress = null;

    /**
     * @return the partyId
     */
    public Long getPartyId() {
        return partyId;
    }

    /**
     * @param partyId the partyId to set
     */
    public void setPartyId(Long partyId) {
        this.partyId = partyId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the locAddress
     */
    public String getLocAddress() {
        return locAddress;
    }

    /**
     * @param locAddress the locAddress to set
     */
    public void setLocAddress(String locAddress) {
        this.locAddress = locAddress;
    }

    /**
     * @return the intAddress
     */
    public String getIntAddress() {
        return intAddress;
    }

    /**
     * @param intAddress the intAddress to set
     */
    public void setIntAddress(String intAddress) {
        this.intAddress = intAddress;
    }
    
    
}
