/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsv.road.operationaladdress.service;

import com.dsv.road.customerorder.model.Address;
import com.dsv.road.customerorder.model.CashOnDelivery;
import com.dsv.road.customerorder.model.DateType;
import com.dsv.road.customerorder.model.Goods;
import com.dsv.road.customerorder.model.GoodsDetails;
import com.dsv.road.customerorder.model.InstructionType;
import com.dsv.road.customerorder.model.Name;
import com.dsv.road.customerorder.model.Order;
import com.dsv.road.customerorder.model.Party;
import com.dsv.road.customerorder.model.Reference;
import com.dsv.road.customerorder.model.TermsOfDelivery;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

/**
 *
 * @author jemun
 */
@ManagedBean(name="orderService")
@ApplicationScoped
public class OrderService {
    
    public List<String> getPackageTypes() {
        ArrayList<String> packageTypes = new ArrayList<String>();
        packageTypes.add("EUR");
        packageTypes.add("CLL");
        packageTypes.add("CRATE");
        return packageTypes;
    }
    public Order getDummyOrder() {
        Order o = new Order("My unique order id ");

        o.addReference(new Reference("External id", "external id "));
        o.addReference(new Reference("Dinas id", "Dinas id "));

        o.setConsigneeParty(new Party(1, new Name("ConsigneeParty on order ")));
        o.getConsigneeParty().setAddress(new Address("DK", "Consignee address"));
        o.setDeliverParty(new Party(2, new Name("DeliverParty on order ")));
        o.getDeliverParty().setAddress(new Address("DK", "Delivery address"));
        o.setPickupParty(new Party(3, new Name("PickupParty on order ")));
        o.getPickupParty().setAddress(new Address("DK", "Pickup address"));
        o.setShipperParty(new Party(4, new Name("ShipperParty on order ")));
        o.getShipperParty().setAddress(new Address("DK", "Shipper address"));

        GoodsDetails gd = new GoodsDetails(1000, "EUR", "Test delivery");
        gd.setGrossVolume(new BigDecimal(100));
        gd.setGrossWeight(new BigDecimal(50));
        gd.setHeight(new BigDecimal(1000));
        gd.setWidth(new BigDecimal(200));
        gd.setLoadingMeters(new BigDecimal(1));
        Goods g = new Goods(gd);

        gd = new GoodsDetails(500, "EUR", "Vacum cleaners");
        gd.setGrossVolume(new BigDecimal(50));
        gd.setGrossWeight(new BigDecimal(25));
        gd.setHeight(new BigDecimal(500));
        gd.setWidth(new BigDecimal(200));
        gd.setLoadingMeters(new BigDecimal(0.5));
        g.addGoodsDetails(gd);
        gd = new GoodsDetails(500, "CLL", "Pipes");
        gd.setGrossVolume(new BigDecimal(50));
        gd.setGrossWeight(new BigDecimal(25));
        gd.setHeight(new BigDecimal(500));
        gd.setWidth(new BigDecimal(200));
        gd.setLoadingMeters(new BigDecimal(0.5));
        g.addGoodsDetails(gd);
        o.addGoods(g);
        gd = new GoodsDetails(1000, "Pallets", "Summary");
        gd.setGrossVolume(new BigDecimal(100));
        gd.setGrossWeight(new BigDecimal(50));
        gd.setHeight(new BigDecimal(1000));
        gd.setWidth(new BigDecimal(200));
        gd.setLoadingMeters(new BigDecimal(1));
        o.setGoodsSummary(gd);

        o.setCashOnDelivery(new CashOnDelivery("DKK", new BigDecimal(1000)));
        o.setTermsOfDelivery(new TermsOfDelivery("Shipper", "Reponsible until delivery"));

        o.setPickupDate(new DateType(new LocalDate(2015, 4, 1), new LocalTime(8, 0), new LocalTime(16, 0)));
        o.setDeliverDate(new DateType(new LocalDate(2015, 5, 1), new LocalTime(8, 0), new LocalTime(16, 0)));

        InstructionType it = new InstructionType();
        it.addInstruction("Call 12345678 30 minutes before");
        it.addInstruction("Call shipper after to confirm cash on delivery payment");
        o.setDeliverInstructions(it);

        it = new InstructionType();
        it.addInstruction("Peter knows where the goods are. Call him at 12345678");
        it.addInstruction("Incase Peter is ill, call Hans at 87654321");
        o.setPickupInstructions(it);
        return o;
    }
}
