/**
 * 
 */
package com.dsv.road.operationaladdress.xml2;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author ext.jesper.munkholm
 * 
 */
@XmlRootElement(name = "Party", namespace = "com.dsv.road.operationaladdress.xml2")
@XmlAccessorType(XmlAccessType.FIELD)
public class Party {

	@XmlElement(name = "PartyId")
	protected long partyId;
	@XmlElement(name = "Name")
	protected Name name;
	@XmlElement(name = "NameInternational")
	protected NameInternational nameInternational;
	@XmlElement(name = "LegalEntity")
	protected LegalEntity legalEntity;
	@XmlElement(name = "Address")
	protected Address address;
	@XmlElement(name = "AddressInternational")
	protected AddressInternational addressInternational;
	// should this be a list, since it has a valid from date
	// @XmlElementWrapper(name="AccessGeneralList")
	@XmlElement(name = "AccessGeneral")
	protected AccessType accessGeneral;
	@XmlElement(name = "AccessPickup")
	protected AccessType accessPickup;
	@XmlElement(name = "AccessDeliver")
	protected AccessType accessDeliver;
	@XmlElementWrapper(name = "Contacts")
	@XmlElement(name = "Contact")
	protected List<Contact> contacts;
	@XmlElementWrapper(name = "Instructions")
	@XmlElement(name = "Instruction")
	protected List<Instruction> instructions;
	@XmlElementWrapper(name = "CustomDetails")
	@XmlElement(name = "CustomDetail")
	protected List<CustomsDetails> customsDetails;

	/**
	 * @return the partyId
	 */
	public long getPartyId() {
		return partyId;
	}

	/**
	 * @param partyId
	 *            the partyId to set
	 */
	public void setPartyId(long partyId) {
		this.partyId = partyId;
	}

	/**
	 * @return the name
	 */
	public Name getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(Name name) {
		this.name = name;
	}

	/**
	 * @return the nameInternational
	 */
	public NameInternational getNameInternational() {
		return nameInternational;
	}

	/**
	 * @param nameInternational
	 *            the nameInternational to set
	 */
	public void setNameInternational(NameInternational nameInternational) {
		this.nameInternational = nameInternational;
	}

	/**
	 * @return the legalEntity
	 */
	public LegalEntity getLegalEntity() {
		return legalEntity;
	}

	/**
	 * @param legalEntity
	 *            the legalEntity to set
	 */
	public void setLegalEntity(LegalEntity legalEntity) {
		this.legalEntity = legalEntity;
	}

	/**
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(Address address) {
		this.address = address;
	}

	/**
	 * @return the addressInternational
	 */
	public AddressInternational getAddressInternational() {
		return addressInternational;
	}

	/**
	 * @param addressInternational
	 *            the addressInternational to set
	 */
	public void setAddressInternational(
			AddressInternational addressInternational) {
		this.addressInternational = addressInternational;
	}

	/**
	 * @return the accessGeneral
	 */
	public AccessType getAccessGeneral() {
		return accessGeneral;
	}

	/**
	 * @param accessGeneral
	 *            the accessGeneral to set
	 */
	public void setAccessGeneral(AccessType accessGeneral) {
		this.accessGeneral = accessGeneral;
	}

	/**
	 * @return the accessPickup
	 */
	public AccessType getAccessPickup() {
		return accessPickup;
	}

	/**
	 * @param accessPickup
	 *            the accessPickup to set
	 */
	public void setAccessPickup(AccessType accessPickup) {
		this.accessPickup = accessPickup;
	}

	/**
	 * @return the accessDeliver
	 */
	public AccessType getAccessDeliver() {
		return accessDeliver;
	}

	/**
	 * @param accessDeliver
	 *            the accessDeliver to set
	 */
	public void setAccessDeliver(AccessType accessDeliver) {
		this.accessDeliver = accessDeliver;
	}

	/**
	 * @return the contacts
	 */
	public List<Contact> getContacts() {
		return contacts;
	}

	/**
	 * @param contact
	 *            the contacts to set
	 */
	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

	/**
	 * @return the instructions
	 */
	public List<Instruction> getInstructions() {
		return instructions;
	}

	/**
	 * @param instruction
	 *            the instructions to set
	 */
	public void setInstructions(List<Instruction> instructions) {
		this.instructions = instructions;
	}

	/**
	 * @return the customsDetails
	 */
	public List<CustomsDetails> getCustomsDetails() {
		return customsDetails;
	}

	/**
	 * @param customsDetails
	 *            the customsDetails to set
	 */
	public void setCustomsDetails(List<CustomsDetails> customsDetails) {
		this.customsDetails = customsDetails;
	}

	/**
	 * @param contact
	 */
	public void addContact(Contact contact) {
		if (contacts == null) {
			contacts = new ArrayList<Contact>();
		}

		contacts.add(contact);
	}

	/**
	 * @param instruction
	 */
	public void addInstruction(Instruction instruction) {
		if (instructions == null) {
			instructions = new ArrayList<Instruction>();
		}

		instructions.add(instruction);
	}

	/**
	 * @param customsDetail
	 */
	public void addCustomsDetail(CustomsDetails customsDetail) {
		if (customsDetails == null)
			customsDetails = new ArrayList<CustomsDetails>();

		customsDetails.add(customsDetail);

	}

	public static boolean saveToFile(Party object, File destinationFile)
			throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(Party.class);
		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		// Write to System.out
		m.marshal(object, destinationFile);
		return true;
	}

	public static Party readFromFile(File sourceFile) throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(Party.class);
		Unmarshaller um = context.createUnmarshaller();
		Party tmp = (Party) um.unmarshal(sourceFile);
		return tmp;
	}

	public static boolean saveToJSONFile(Party object, File destinationFile)
			throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(Party.class);
		Marshaller m = context.createMarshaller();
		//m.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
		//m.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		// Write to System.out
		m.marshal(object, destinationFile);
		return true;
	}

	public static Party readFromJSONFile(File sourceFile) throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(Party.class);
		Unmarshaller um = context.createUnmarshaller();
		Party tmp = (Party) um.unmarshal(sourceFile);
		return tmp;
	}

}
