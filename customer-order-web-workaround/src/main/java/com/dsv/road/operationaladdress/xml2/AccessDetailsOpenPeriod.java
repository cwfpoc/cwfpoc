/**
 * 
 */
package com.dsv.road.operationaladdress.xml2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.joda.time.LocalTime;

/**
 * @author ext.jesper.munkholm
 *
 */
@XmlRootElement(name = "AccessDetailsOpenPeriod", namespace = "com.dsv.road.operationaladdress.xml2")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccessDetailsOpenPeriod {

	@XmlElement(name = "OpenAt", required = true)
	@XmlJavaTypeAdapter(LocalTimeAdapter.class)
    protected LocalTime openAt;
    @XmlElement(name = "ClosedAt", required = true)
    @XmlJavaTypeAdapter(LocalTimeAdapter.class)
    protected LocalTime closedAt;
    
    /**
     * 
     */
    public AccessDetailsOpenPeriod() {
    	
    }

	/**
	 * @param openAt
	 * @param closedAt
	 */
	public AccessDetailsOpenPeriod(LocalTime openAt, LocalTime closedAt) {
		this.openAt = openAt;
		this.closedAt = closedAt;
	}

	/**
	 * @return the openAt
	 */
	public LocalTime getOpenAt() {
		return openAt;
	}

	/**
	 * @param openAt the openAt to set
	 */
	public void setOpenAt(LocalTime openAt) {
		this.openAt = openAt;
	}

	/**
	 * @return the closedAt
	 */
	public LocalTime getClosedAt() {
		return closedAt;
	}

	/**
	 * @param closedAt the closedAt to set
	 */
	public void setClosedAt(LocalTime closedAt) {
		this.closedAt = closedAt;
	}

	
}
