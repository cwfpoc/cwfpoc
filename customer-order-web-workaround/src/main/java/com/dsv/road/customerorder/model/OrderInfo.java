package com.dsv.road.customerorder.model;

import java.io.Serializable;

public class OrderInfo implements Serializable {

	private static final long serialVersionUID = 6303188575735552416L;

	private String orderId;
	private String shipperPartyName;
	private String consigneePartyName;
	private String pickupDate;
	private String deliverDate;
	private String goodsSummary;
	
	public OrderInfo() {
		
	}
	
	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return orderId;
	}
	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	/**
	 * @return the shipperPartyName
	 */
	public String getShipperPartyName() {
		return shipperPartyName;
	}
	/**
	 * @param shipperPartyName the shipperPartyName to set
	 */
	public void setShipperPartyName(String shipperPartyName) {
		this.shipperPartyName = shipperPartyName;
	}
	/**
	 * @return the consigneePartyName
	 */
	public String getConsigneePartyName() {
		return consigneePartyName;
	}
	/**
	 * @param consigneePartyName the consigneePartyName to set
	 */
	public void setConsigneePartyName(String consigneePartyName) {
		this.consigneePartyName = consigneePartyName;
	}
	/**
	 * @return the pickupDate
	 */
	public String getPickupDate() {
		return pickupDate;
	}
	/**
	 * @param pickupDate the pickupDate to set
	 */
	public void setPickupDate(String pickupDate) {
		this.pickupDate = pickupDate;
	}
	/**
	 * @return the deliverDate
	 */
	public String getDeliverDate() {
		return deliverDate;
	}
	/**
	 * @param deliverDate the deliverDate to set
	 */
	public void setDeliverDate(String deliverDate) {
		this.deliverDate = deliverDate;
	}
	/**
	 * @return the goodsSummary
	 */
	public String getGoodsSummary() {
		return goodsSummary;
	}
	/**
	 * @param goodsSummary the goodsSummary to set
	 */
	public void setGoodsSummary(String goodsSummary) {
		this.goodsSummary = goodsSummary;
	}
	
	
	
}
