/**
 *
 */
package com.dsv.road.customerorder.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ext.jesper.munkholm
 *
 */
public class Goods {

    protected GoodsDetails goodsDetails;
    protected ArrayList<GoodsDetails> goodsContainedList;

    /**
     *
     */
    public Goods() {
        super();
        goodsContainedList = new ArrayList<GoodsDetails>();
    }

    /**
     * @param goodsDetails
     */
    public Goods(GoodsDetails goodsDetails) {
        super();
        goodsContainedList = new ArrayList<GoodsDetails>();
        this.goodsDetails = goodsDetails;
    }

    /**
     * Gets the value of the goodsDetails property.
     *
     * @return possible object is {@link GoodsDetails }
     *
     */
    public GoodsDetails getGoodsDetails() {
        return goodsDetails;
    }

    /**
     * Sets the value of the goodsDetails property.
     *
     * @param value allowed object is {@link GoodsDetails }
     *
     */
    public void setGoodsDetails(GoodsDetails value) {
        this.goodsDetails = value;
    }

    /**
     * @return
     */
    public List<GoodsDetails> getGoodsContainedList() {
        if (goodsContainedList == null) {
            goodsContainedList = new ArrayList<GoodsDetails>();
        }

        return goodsContainedList;
    }

    /**
     * @param goodsContainedList
     */
    public void setGoodsContainedList(ArrayList<GoodsDetails> goodsContainedList) {
        this.goodsContainedList = goodsContainedList;
    }

    public void addGoodsDetails(GoodsDetails goodsDetails) {
        if (goodsContainedList == null) {
            goodsContainedList = new ArrayList<GoodsDetails>();
        }

        goodsContainedList.add(goodsDetails);
    }
    
    public int getGoodsContainedListSize() {
        if (goodsContainedList == null) {
            return 0;
        } 
        
        return goodsContainedList.size();
    }
}
