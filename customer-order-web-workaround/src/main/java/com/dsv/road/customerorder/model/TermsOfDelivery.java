/**
 * 
 */
package com.dsv.road.customerorder.model;

/**
 * @author ext.jesper.munkholm
 *
 */
public class TermsOfDelivery {

    protected String term;
    protected String location;

    
    /**
	 * 
	 */
	public TermsOfDelivery() {
		super();
	}

	/**
	 * @param term
	 * @param location
	 */
	public TermsOfDelivery(String term, String location) {
		super();
		this.term = term;
		this.location = location;
	}

	/**
     * Gets the value of the term property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTerm() {
        return term;
    }

    /**
     * Sets the value of the term property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTerm(String value) {
        this.term = value;
    }

    /**
     * Gets the value of the location property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocation() {
        return location;
    }

    /**
     * Sets the value of the location property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocation(String value) {
        this.location = value;
    }

}
