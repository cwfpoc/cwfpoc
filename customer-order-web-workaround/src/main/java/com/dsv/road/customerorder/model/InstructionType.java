/**
 * 
 */
package com.dsv.road.customerorder.model;

import java.util.ArrayList;

/**
 * @author ext.jesper.munkholm
 *
 */
public class InstructionType {

	protected ArrayList<String> instructionList;

	/**
	 * 
	 */
	public InstructionType() {
		super();
	}

	/**
	 * @return the instructionList
	 */
	public ArrayList<String> getInstructionList() {
		return instructionList;
	}

	/**
	 * @param instructionList the instructionList to set
	 */
	public void setInstructionList(ArrayList<String> instructionList) {
		this.instructionList = instructionList;
	}
	
	public void addInstruction(String instruction) {
		if (instructionList == null)
			instructionList = new ArrayList<String>();
		
		instructionList.add(instruction);
	}
	
	
	
}
