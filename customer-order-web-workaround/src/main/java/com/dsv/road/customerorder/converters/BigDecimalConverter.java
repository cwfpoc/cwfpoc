/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsv.road.customerorder.converters;

import java.math.BigDecimal;
import java.math.RoundingMode;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author jemun
 */
@FacesConverter("com.dsv.road.customerorder.converters.BigDecimalConverter")
public class BigDecimalConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.stripTrailingZeros();
        if (bd.scale() > 3)
            return bd.setScale(3, RoundingMode.UP);
        else
            return bd;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        BigDecimal bd = (BigDecimal)value;
        
        String result = bd.toPlainString();
        result = !result.contains(".") ? result : result.replaceAll("0*$", "").replaceAll("\\.$", "");
        return result;
    }
    
}
