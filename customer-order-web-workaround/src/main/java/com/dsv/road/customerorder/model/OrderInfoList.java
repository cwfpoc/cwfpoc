/**
 * 
 */
package com.dsv.road.customerorder.model;

import java.util.ArrayList;

/**
 * @author ext.jesper.munkholm
 *
 */
public class OrderInfoList {

	ArrayList<OrderInfo> list;
	
	/**
	 * 
	 */
	public OrderInfoList() {
	}

	/**
	 * @return the list
	 */
	public ArrayList<OrderInfo> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList<OrderInfo> list) {
		this.list = list;
	}
	
	
}
